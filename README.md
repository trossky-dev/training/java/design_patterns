# Design Patterns in Java
## Chapter 01: POO
## Chapter 02: SOLID
```java
/***********************************************************************************************************************
 *                                                    SOLID
 * *********************************************************************************************************************
 *                                    S - Single Responsibility Principle (SRP)
 * COHESIÓN: La cohesión es una medida de
 *          cuantó una unidad tiene relación
 *          con sigo mismas.
 * examples
 *          1. La clase Util, tendrá una cohesión BAJA,
 *          2. Una clase que realiza una tarea en concreto, es de cohesión ALTA
 *
 * -- [SRP]-Single Responsibility Principle --
 *                        A class should have one and only one reason to change,
 *                        meaning that a class should have only one job.
 *
 * -- [OCP]-Open/Close Principle --
 *                       Objects or entities should be open for extension,
 *                       but closed for modification.
 *                       example: ver {@link IFigura } y {@link Presentacion}
 *                       Si queremos extender nuestra aplicación, se debe lograr
 *                       SIN modificar el codigo ya existente.
 *                       Esto suele resolverse con la herencia.
 *
 * -- [LSP] Liskov Substitution Principle --  name is by Barbara Liskov(Ing. Software).
 *                      Let q(x) be a property provable about objects of x of type T.
 *                      Then q(y) should be provable for objects y of type S, where S is a subtype of T.
 *                      Los subtipos deben ser substituibles por subtipos base.
 *                      example:
 *                      Jaguar subtipo de Felino
 *                      Opc.1: Jaguar yagua= new Jaguar()
 *                      Opc.2: Felino yagua= new Jaguar()
 *                      Pero esta implemnetación que hicimos antes, No  cumple el principio de Liskov,
 *                      ya que debería permitir usar todos los Métodos de la clase padre{@link IFelino}
 *                      en este caso el método yagua2.maullar() genera una excepción que hemos programado antes.
 *                      Para resolverse se requiere del siguiente principio SOLID.
 *
 * -- [ISP] Interface Segregation Principle --
 *                      A client should never be forced to implement an interface that it does not use
 *                      or clients shouldn't be forced to depend on methods they do not use.
 *                      FAT INTERFACE: Cuando se definen métodos mas de lo debido.
 *                      example:
 *                      Jaguar subtipo de Felino
 *                      Opc.2: Felino yagua2= new Jaguar()
 *                      yagua2.maullar(),
 *                      Requiere de una Segregación de Interfaces, osea hacer una abstración mas alta.
 *
 * -- [DIP]  Dependency Inversion Principle --
 *                      Entities must depend on abstractions not on concretions. It state that the high level module
 *                      must not depend on the low level module, but they should depend on abstractions.
 *                      Los modulos de alto nivel, no debe depender de un modulo de bajo Nivel, si no que debe depender
 *                      de abstracciones.
 *
 */
```
## Chapter 03: Design Patterns- Creational Patterns
```java
/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR CREATING OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [SF]-Simple Factory --
 *               No es considerado como un patron de diseño, pero sirve para la creación de objetos complejos.
 *               O para conrolar la creación de los objetos.
 *               Example:
 *               See class clase {@link Pizzeria}
 *
 * -- [FM]-Factory Method --
 *                        Define an interface for creating an object,
 *                        but let subclasses decide which class to instantiate.
 *                        Factory Method lets a class defer instantiation to subclasses.
 *                        Example:
 *                        Nos permite crear objetos complejos de una forma controlada algo así como Simple Facotry,
 *                        la diferencia radica que en este patron vamos a usar una interface.
 *                        See class {@link IPizzeria}, {@link PizzeriaTKY} , {@link PizzaOrillaRellena}
 *
 * -- [FM]-Abstract Factory --
 *                        Provide an interface for creating families of related or dependent objects
 *                        without specifying their concrete classes.
 *                        Example:
 *                        Please see:
 *                          {@link IAbstractFactory}
 *                                -> {@link IComputadora} Definition Family
 *                                -> {@link ITablet} Definition Family
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link MacBookPro} from family {@link IComputadora}
 *                                -> {@link Ipad} from family {@link ITablet}
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link QX410} from family  {@link IComputadora}
 *                                -> {@link TabS3} from family {@link ITablet}
 *
 * -- [S]-Singleton --
 *                        Ensure a class only has one instance, and provide a global point of access to it.
 *                        Example:
 *                        La implementación de este patron es relativamente sencillo, PERO se debe validar
 *                        los errores que pueden surgir, más cuando hay temas de concurrencia.
 *
 *                        Se usa este patron siempre que necesitemos una sola instancia en nuestra applicación,
 *                        como lo es un carro de compras en nuestra aplicación, unicamente va existir un unico carro.
 *
 *                        Otro ejemplo, es el acceso a bases de datos, en caso como Android, podemos tener N cantidad
 *                        de Activities, y todas ellas requiere acceso a la base datos, entonces unicamente se crea
 *                        una conexión a base de datos.
 *                        Tambien el manejo de sessión, Cokies, entre otras.
 * -- hashCode() --
 *              Devuelve un hash unico para cada objeto.
 *
 * -- [SC]-Singleton Concurrent --
 *                        Singleton, but the Class is concurrent now, So You can use connections to DB using
 *                        thread without problem because It class now is using -- SYNCHRONIZED --
 *                        Example:
 *                        See class {@link ConexionDB}
 *
 * -- [B]-Builder --
 *                        Separate the construction of a complex object from its representation so that the same
 *                        construction process can create  different representation.
 *                        Example
 *                        See class {@link User} and {@link User.BuilderUser}
 *                        Add conditional to create a object.
 */
```
## Chapter 04: Design Patterns- Structural patterns
```java
/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR STRUCTURE OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [A]-Adapter --
 *               Convert the interface of a class into another interface clients expect.
 *               Adapter lets classes works together that cloud not otherwise because of
 *               incompatible interfaces.
 *               Example:
 *               Permite extender la applicación sin modificar la app.
 *               See class {@link AdapterDB}
 *
 * -- [C]-Composite --
 *               Compose objects into tree structures to represent part-whole hierarchies.
 *               Composite lets clients treat individual objects and compositions
 *               of objects uniformly.
 *               Example:
 *               See class {@link Menu}
 *
 * -- [D]-Decorator --
 *               Attach additional responsibilities  to an object dynamically.
 *               Decorators provide a flexible alternative to Subclassing for
 *               extending functionally.
 *               Example:
 *               See class {@link QuesoExtra} {@link OrillaRellena}
 * -- [F]-Facade --
 *               Provide a unified interface to a set of interfaces in a subsystem.
 *               Facade defines a higher-level interface that makes the subsystem
 *               easier to use.
 *               Example:
 *               See class {@link Fachada}
 *
 * -- [P]-Proxy --
 *               Provide a surrogate or placeholder for another object to
 *               control access to it.
 *               Example:
 *               Te permite limitar funcionalidades de la app,
 *               por seguridad debemos limitar el acceso a ciertas partes
 *               del sistema
 *               See class {@link ProxyService}
 * -- [VP]-Virtual Proxy --
 *               In place of a complex or heavy object, a skeleton representation may be advantageous in some cases.
 *               When an underlying image is huge in size, it may be represented using a virtual proxy object,
 *               loading the real object on demand {https://en.wikipedia.org/wiki/Proxy_pattern}.
 *               Example:
 *               No restringe el acceso al objeto real, busca retrazar la creación
 *               del objeto, suele aplicarse en casos donde la creación del objeto
 *               tiene un alto costo computacional.
 *               Cuando un Objeto se crea apartir de una consulta a (BD,API o File).
 *               Entonces busca crear el objeto unicamente cuando se necesite.
 *               See class {@link }
 *
 * -- [FLY]-Flyweight --
 *               Use sharing to support large  numbers of fine-grained objects efficiently.
 *               Example:
 *               You can re use objects to that you create application with minimum resources necessaries.
 *               So you can build a flyweight app.
 *               See class {@link }
 */
```
## Chapter 05: Design Patterns- Structural patterns
```java
/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR BEHAVIORAL OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [CR]-Chain Responsibility --
 *               Avoid coupling the sender of a request ti its receiver by giving more than one object a
 *               change to handle the resquest.
 *               Chainthe receiving objects and pass the resquest along the chain until an object handles it.
 *               Example:
 *               Permite establecer una cadena de objetos receptores, la cual debe ser capaz de recibir
 *               información de un objeto emisor.
 *               Es decir el Cliente envia la petición al objeto número 1.
 *                  Sí el objeto número 1, no es capaz de responder a la petición, entonces delegara la tarea
 *                  al objeto número 2.
 *                  Ahora el Objeto número 2 intentara responder de forma correcta, pero si falla entonces delegara
 *                  al Objeto número 3. Y asi sucesivamente se delegara la responsabilidad hasta que algún objeto
 *                  logre responder satisfactoriamente.TR
 *               See class {@link ITransactionHandler}
 *
 * -- [C]-Command --
 *               Encapsulate a request as an object, thereby letting parameterize clients with different requests,
 *               queue or log requests, and support undoable operations.
 *               Example:
 *               Desacoplar al objeto que invoca la operaciòn.
 *               See class {@link ICommand}
 *
 * -- [I]-Iterator --
 *               Provide a way to access the elements of an aggregate object sequentially without exposing
 *               its underlying representation.
 *               Example:
 *               Para no exponer los atributos y metodos de la colección.
 *               See class {@link }
 *
 * -- [M]-Mediator --
 *               Define an object that encapsulates hw a set of objects interact.
 *               Mediator promotes loose coupling by keeping objects from referring
 *               to each explicity, and it lets you vary their interaction independently.
 *               Example:
 *               Una torre de control en un aeropuerto, la torre de control es el mediador para
 *               que los aviones essten en comunicación
 *               See class {@link RoomChat}
 *
 * -- [ME]-Memento --
 *               Without violating encapsulation, capture and externalize an object's internal state
 *               so that the object can be restored to this state later.
 *               Example:
 *               Se puede implementar el Ctl+z en nuestra app. Le permite crear una copia de seguridad
 *               completa o parcial del objeto para posteriormente poder revertir cambios.
 *               See class {@link com.trossky.design_patterns.chapter05.part05_memento.User}
 *
 * -- [O]-Observer --
 *               Define a one-to-many dependency between objects so that when one object change state,
 *               all its dependents are notified and updated automatically.
 *               Example:
 *               Siempre que un producto se venda, sera notificada a todos los administradores
 *               Tambien conocido como --PUBSUB--
 *               See class {@link IObserver} {@link IObservable}
 *
 * -- [S]-State --
 *               Allow an object to alter its behavior when its internal state changes.
 *               The object will appear to change its class.
 *               Example:
 *               Only when you require state machine in your projects.
 *               This example use car object, It have the states as: Start, in Movement and Off.
 *               See class {@link ICarState} {@link CarStart} {@link CarMovement} {@link CarOff}
 *
 * -- [ST]-Strategy --
 *               Define a family of algorithms, encapsulate each one, and make them interchangeable.
 *               Strategy lets the algorithm vary independently from clients taht use it.
 *               Example:
 *               Encapsulate algorithm in class, as a part of lego. In runtime.
 *               This patterns is used when an algorithm change lot of in an system.
 *               Operations for calculate commisiones, Its change for each user.
 *               Note: An Class for each algorithm. this example have two algorithm.
 *               See class {@link IStrategy} {@link Deposit} {@link Withdrawal}
 * -- [TM]-Template Method --
 *               Define the skeleton of an algorithm in an operation, deferring some steps
 *               to subclasses. Template Method lets subclasses redefine certain steps of
 *               an algorithm without changing the algorithm's structure.
 *               Example:
 *               If you already use abstract class, so You already use this pattern.
 *               See class {@link User} {@link Administrator} {@link Manager}
 *
 * -- [V]-Visitor --
 *               Represent an operation to be performed on the elements of una object structure.
 *               Visitor lets you define a new operation without changing classes of the
 *               elements on which it operates.
 *               Example:
 *               Separate an algorithm of an object, If a object is responsible of manage
 *               some information type, so too is responsible for do all operations necessary over
 *               its information.
 *               For this example use a small supermarket with  discount to some products
 *               See class {@link } {@link } {@link }
 */
```

From course of [CF](https://codigofacilito.com/cursos/patrones-diseno)
