package com.trossky.design_patterns.chapter01;

public class  Animal {
  public void comer() {
    System.out.println("EL animal come");
  }
  public void dormir() {
    System.out.println("EL animal duerme");
  }
}
