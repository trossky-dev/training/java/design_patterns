package com.trossky.design_patterns.chapter01;

public interface IFelinoSalvaje extends IFelino{

  void rugir();
}
