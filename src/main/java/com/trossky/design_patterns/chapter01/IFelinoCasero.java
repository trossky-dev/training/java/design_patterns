package com.trossky.design_patterns.chapter01;

public interface IFelinoCasero extends IFelino{
  void maullar() ;
}
