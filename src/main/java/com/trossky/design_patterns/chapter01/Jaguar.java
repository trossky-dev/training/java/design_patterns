package com.trossky.design_patterns.chapter01;

/**
 * EL polimorfismo lo podemos asociar al termino, sobreescribir metodos.
 * */
public class Jaguar  extends Animal implements IFelinoSalvaje{
  private int edad;
  private float peso;

  public Jaguar() {
    this.setEdad(0);
    this.setPeso(0);
  }

  public Jaguar(int edad, float peso) {
    this.setEdad( edad);
    this.setPeso(peso);
  }
  public Jaguar(int edad) {
    this.setEdad( edad);
    this.setPeso(0);
  }
  public Jaguar(float peso) {
    this.setEdad( 0);
    this.setPeso(peso);
  }

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  public float getPeso() {
    return peso;
  }

  public void setPeso(float peso) {
    this.peso = peso;
  }

  public String toString() {
    return "Jaguar{" +
        "edad=" + edad +
        ", peso=" + peso +
        '}';
  }
  public void dormir() {
    System.out.println("El Jaguar duerme!");
  }

  public void comer() {
    System.out.println("El Jaguar come!");
  }

  public void rugir() {
    System.out.println("El Jaguar Ruge!");
  }

  public void cazar() {
    System.out.println("El Jaguar caza!");
  }
}
