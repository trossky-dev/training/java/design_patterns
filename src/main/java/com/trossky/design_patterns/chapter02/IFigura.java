package com.trossky.design_patterns.chapter02;

/**
 * Open/Close principle applying
 * */
public interface IFigura {
  float area();
}
