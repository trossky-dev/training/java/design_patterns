package com.trossky.design_patterns.chapter02;

public class Triangulo  implements  IFigura{
  private float base;
  private float altura;

  public Triangulo(float base, float altura) {
    this.setBase(base);
    this.setAltura(altura);
  }

  public float getBase() {
    return base;
  }

  public void setBase(float base) {
    this.base = base;
  }

  public float getAltura() {
    return altura;
  }

  public void setAltura(float altura) {
    this.altura = altura;
  }

  @Override
  public String toString() {
    return "Rectangulo{" +
        "Base " + base +
        ", Altura=" + altura +
        '}';
  }
  public float area() {
    return (this.getBase() * this.getAltura())/2;
  }

}
