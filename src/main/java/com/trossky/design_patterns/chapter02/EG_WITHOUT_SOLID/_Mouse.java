package com.trossky.design_patterns.chapter02.EG_WITHOUT_SOLID;

/**
 * This class is low module
 */
public class _Mouse {

  public void conectar(){
    System.out.println("Conexión _Mouse vía USB!");
  }
}
