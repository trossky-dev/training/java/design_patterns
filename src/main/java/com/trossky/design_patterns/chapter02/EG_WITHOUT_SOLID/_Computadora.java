package com.trossky.design_patterns.chapter02.EG_WITHOUT_SOLID;

/**
 * This class is high level module
 */
public class _Computadora {

  private _Mouse mouse;
  private _Teclado teclado;

  public _Computadora() {
    this.mouse = new _Mouse();
    this.teclado = new _Teclado();
  }
  public void encender() {
    this.teclado.conectar();
    this.mouse.conectar();
  }
}
