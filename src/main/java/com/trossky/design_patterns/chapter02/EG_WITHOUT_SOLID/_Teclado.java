package com.trossky.design_patterns.chapter02.EG_WITHOUT_SOLID;

/**
 * This class is low level module
 */
public class _Teclado {

  public void conectar(){
    System.out.println("Conexión _Teclado vía USB!");
  }
}
