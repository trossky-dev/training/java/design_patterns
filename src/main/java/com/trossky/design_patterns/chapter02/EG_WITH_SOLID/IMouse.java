package com.trossky.design_patterns.chapter02.EG_WITH_SOLID;

import com.trossky.design_patterns.chapter02.EG_WITH_SOLID.IComponente;

public interface IMouse extends IComponente {
}
