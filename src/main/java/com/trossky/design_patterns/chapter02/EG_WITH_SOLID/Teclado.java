package com.trossky.design_patterns.chapter02.EG_WITH_SOLID;

/**
 * This class is low level module
 */
public class Teclado implements ITeclado {

  public void conectar(){
    System.out.println("Conexión Teclado vía USB!");
  }
}
