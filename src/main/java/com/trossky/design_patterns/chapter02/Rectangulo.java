package com.trossky.design_patterns.chapter02;

/**
 * Tiene ALTA COHESIÓN,
 * Porque realiza tareas muy puntuales y tiene alta relación consigo misma.
 *
 * Esta clase responde a la sig. pregunta:
 * ¿ -- QUÉ -- SE DEBE MOSTRAR?
 */
public class Rectangulo implements  IFigura{
  private float base;
  private float altura;

  public Rectangulo(float base, float altura) {
    this.setBase(base);
    this.setAltura(altura);
  }

  public float getBase() {
    return base;
  }

  public void setBase(float base) {
    this.base = base;
  }

  public float getAltura() {
    return altura;
  }

  public void setAltura(float altura) {
    this.altura = altura;
  }

  @Override
  public String toString() {
    return "Rectangulo{" +
        "Base " + base +
        ", Altura=" + altura +
        '}';
  }
  public float area() {
    return this.getBase() * this.getAltura();
  }

  /**
   * FIXME: Esta funcionalidad no cumple con el Primer Principio de Resposabilidad Unica,
           Ya que esta funcionalidad debe estar en la capa de Presentación.
   * -- IMPORTANT --
   * Si se deja acá, que pasa si en una funcionalidad posterior se requiere escribir la información en un archivo,
   * en la web... Tendriamos que incluir la funcionalidad acá. Lo cual no debe ser así.
   * ver la clase {@link Presentacion }
   */
  @Deprecated
  public void imprimir(){
    // System.out.println(this);
  }

}
