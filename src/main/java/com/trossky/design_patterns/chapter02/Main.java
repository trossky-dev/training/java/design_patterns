package com.trossky.design_patterns.chapter02;

import com.trossky.design_patterns.chapter01.IFelino;
import com.trossky.design_patterns.chapter01.IFelinoSalvaje;
import com.trossky.design_patterns.chapter01.Jaguar;
import com.trossky.design_patterns.chapter02.EG_WITHOUT_SOLID._Computadora;
import com.trossky.design_patterns.chapter02.EG_WITH_SOLID.Computadora;
import com.trossky.design_patterns.chapter02.EG_WITH_SOLID.Mouse;
import com.trossky.design_patterns.chapter02.EG_WITH_SOLID.Teclado;

import java.util.logging.Logger;

/***********************************************************************************************************************
 *                                                    SOLID
 * *********************************************************************************************************************
 *                                    S - Single Responsibility Principle (SRP)
 * COHESIÓN: La cohesión es una medida de
 *          cuantó una unidad tiene relación
 *          con sigo mismas.
 * examples
 *          1. La clase Util, tendrá una cohesión BAJA,
 *          2. Una clase que realiza una tarea en concreto, es de cohesión ALTA
 *
 * -- [SRP]-Single Responsibility Principle --
 *                        A class should have one and only one reason to change,
 *                        meaning that a class should have only one job.
 *
 * -- [OCP]-Open/Close Principle --
 *                       Objects or entities should be open for extension,
 *                       but closed for modification.
 *                       example: ver {@link IFigura } y {@link Presentacion}
 *                       Si queremos extender nuestra aplicación, se debe lograr
 *                       SIN modificar el codigo ya existente.
 *                       Esto suele resolverse con la herencia.
 *
 * -- [LSP] Liskov Substitution Principle --  name is by Barbara Liskov(Ing. Software).
 *                      Let q(x) be a property provable about objects of x of type T.
 *                      Then q(y) should be provable for objects y of type S, where S is a subtype of T.
 *                      Los subtipos deben ser substituibles por subtipos base.
 *                      example:
 *                      Jaguar subtipo de Felino
 *                      Opc.1: Jaguar yagua= new Jaguar()
 *                      Opc.2: Felino yagua= new Jaguar()
 *                      Pero esta implemnetación que hicimos antes, No  cumple el principio de Liskov,
 *                      ya que debería permitir usar todos los Métodos de la clase padre{@link IFelino}
 *                      en este caso el método yagua2.maullar() genera una excepción que hemos programado antes.
 *                      Para resolverse se requiere del siguiente principio SOLID.
 *
 * -- [ISP] Interface Segregation Principle --
 *                      A client should never be forced to implement an interface that it does not use
 *                      or clients shouldn't be forced to depend on methods they do not use.
 *                      FAT INTERFACE: Cuando se definen métodos mas de lo debido.
 *                      example:
 *                      Jaguar subtipo de Felino
 *                      Opc.2: Felino yagua2= new Jaguar()
 *                      yagua2.maullar(),
 *                      Requiere de una Segregación de Interfaces, osea hacer una abstración mas alta.
 *
 * -- [DIP]  Dependency Inversion Principle --
 *                      Entities must depend on abstractions not on concretions. It state that the high level module
 *                      must not depend on the low level module, but they should depend on abstractions.
 *                      Los modulos de alto nivel, no debe depender de un modulo de bajo Nivel, si no que debe depender
 *                      de abstracciones.
 *
 */
public class Main {
  private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

  public static void main(String[] args) {
    LOGGER.info("Logger Name: "+LOGGER.getName());
    // Classes whith high cohesión
    System.out.println("START SRP ");
    Presentacion presentacion = new Presentacion();
    Rectangulo rectangulo = new Rectangulo(10,20);
    Triangulo triangulo = new Triangulo(10,5);
    System.out.println("START OCP ");
    presentacion.area(rectangulo);
    presentacion.area(triangulo);

    System.out.println("START LSP ");
    beginWithLiskovSubstitution();

    beginWithDependencyInversionPrinciple();
  }



  private static void beginWithLiskovSubstitution() {
    Jaguar yagua1 = new Jaguar(10,120f);
    System.out.println("START ISP ");

    IFelinoSalvaje yagua2 = new Jaguar(10,120f);
    IFelino yagua3 = new Jaguar(10,120f);
    yagua1.cazar();
    yagua2.cazar();

    /**
     * No se cumple el principio de Liskov,
     * ya que debería permitir usar todos los Métodos de la clase padre{@link IFelino} en este caso el método
     * yagua2.maullar(), genera una excepción que hemos programado antes.
     * Para resolverse se requiere del siguiente principio SOLID.
     * */
    System.out.println("LiskovSubstitution is not already");
    // @Deprecate yagua2.maullar(); // LiskovSubstitution is not already

    System.out.println("Liskov Substitution is already using Interface Segregation Principle");
    yagua1.cazar();
    yagua2.cazar();
    yagua3.cazar();
  }

  private static void beginWithDependencyInversionPrinciple() {
    System.out.println("START EG FINAL ");

    _Computadora _computadora = new _Computadora();
    _computadora.encender();
    System.out.println("START DIP ");
    Mouse mouse=new Mouse();
    Teclado teclado = new Teclado();
    Computadora computadora = new Computadora(mouse,teclado);
    computadora.encender();

  }
}
