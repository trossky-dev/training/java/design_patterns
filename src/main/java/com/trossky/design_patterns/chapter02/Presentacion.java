package com.trossky.design_patterns.chapter02;

/**
 * Tiene ALTA COHESIÓN,
 * Porque realiza tareas muy puntuales y tiene alta relación consigo misma.
 *
 * Esta clase responde a la sig. pregunta:
 * ¿ -- COMÓ -- SE DEBE MOSTRAR?
 */

public class Presentacion {

  public void area(IFigura iFigura){
    System.out.println(iFigura.area());
  }
}
