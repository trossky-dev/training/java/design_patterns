package com.trossky.design_patterns.chapter03.part07_prototype;

/**
 * Tiene ALTA COHESIÓN,
 * Porque realiza tareas muy puntuales y tiene alta relación consigo misma.
 *
 * Esta clase responde a la sig. pregunta:
 * ¿ -- COMÓ -- SE DEBE MOSTRAR?
 */

public class Presentacion {

  public void showEnemies(IEnemigo iEnemigo){
    System.out.println(iEnemigo);
  }
}
