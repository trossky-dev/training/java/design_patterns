package com.trossky.design_patterns.chapter03.part07_prototype;

import com.trossky.design_patterns.chapter03.part01_simple_factory.Pizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.IPizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzaOrillaRellena;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzeriaTKY;
import com.trossky.design_patterns.chapter03.part03_abstract_factory.*;
import com.trossky.design_patterns.chapter03.part05_singleton_concurrente.ConexionDB;
import com.trossky.design_patterns.chapter03.part06_builder_03challenge.User;

/***********************************************************************************************************************
 *                                    PATRONES DE DISEÑO PARA CREACIÓN DE OBJETOS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [SF]-Simple Factory --
 *               No es considerado como un patron de diseño, pero sirve para la creación de objetos complejos.
 *               O para conrolar la creación de los objetos.
 *               Example:
 *               Ver la sig. clase {@link Pizzeria}
 *
 * -- [FM]-Factory Method --
 *                        Define an interface for creating an object,
 *                        but let subclasses decide which class to instantiate.
 *                        Factory Method lets a class defer instantiation to subclasses.
 *                        Example:
 *                        Nos permite crear objetos complejos de una forma controlada algo así como Simple Facotry,
 *                        la diferencia radica que en este patron vamos a usar una interface.
 *                        ver la sig. clase {@link IPizzeria}, {@link PizzeriaTKY} , {@link PizzaOrillaRellena}
 *
 * -- [FM]-Abstract Factory --
 *                        Provide an interface for creating families of related or dependent objects
 *                        without specifying their concrete classes.
 *                        Example:
 *                        Please see:
 *                          {@link IAbstractFactory}
 *                                -> {@link IComputadora} Definition Family
 *                                -> {@link ITablet} Definition Family
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link MacBookPro} from family {@link IComputadora}
 *                                -> {@link Ipad} from family {@link ITablet}
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link QX410} from family  {@link IComputadora}
 *                                -> {@link TabS3} from family {@link ITablet}
 *
 * -- [S]-Singleton --
 *                        Ensure a class only has one instance, and provide a global point of access to it.
 *                        Example:
 *                        La implementación de este patron es relativamente sencillo, PERO se debe validar
 *                        los errores que pueden surgir, más cuando hay temas de concurrencia.
 *
 *                        Se usa este patron siempre que necesitemos una sola instancia en nuestra applicación,
 *                        como lo es un carro de compras en nuestra aplicación, unicamente va existir un unico carro.
 *
 *                        Otro ejemplo, es el acceso a bases de datos, en caso como Android, podemos tener N cantidad
 *                        de Activities, y todas ellas requiere acceso a la base datos, entonces unicamente se crea
 *                        una conexión a base de datos.
 *                        Tambien el manejo de sessión, Cokies, entre otras.
 * -- hashCode() --
 *              Devuelve un hash unico para cada objeto.
 *
 * -- [SC]-Singleton Concurrent --
 *                        Singleton, but the Class is concurrent now, So You can use connections to DB using
 *                        thread without problem because It class now is using -- SYNCHRONIZED --
 *                        Example:
 *                        See class {@link ConexionDB}
 *
 * -- [B]-Builder --
 *                        Separate the construction of a complex object from its representation so that the same
 *                        construction process can create  different representation.
 *                        Example
 *                        See class {@link User} and {@link User.BuilderUser}
 *                        Add conditional to create a object.
 * -- [B]-Prototype --
 *                        Specify the kinds of objects to create using a prototypical instance, and create
 *                        new objects by copying this prototype.
 *                  -Note-
 *                        Podemos tomar un objeto base, y apertir de este, empezar a crear N cantidades de
 *                        nuevos objetos. Estos nuevos objetos son ligeramente diferentes al original.
 *                 -Use Case-
 *                        1. Suele usarse cuando se necesita crear objetos parececidos a otros.
 *                        2. Cuando el valor de un atributo
 *                            es muy costoso obtenerlo(Consulta a base datos, de un API, etc).
 *                        -Example-
 *                         See class {@link Enemigo}
 *                         Recuerde!!! los objetos generados son ligeramente diferentes al original.
 *
 *
 */
public class MPrototype {
  /**
   * Ejemplo practico
   *   item 1.  Enimigo base
   *       _____________
   *      |X           |
   *   item 2. Crear 3 enemigos, aprtir del base
   *      Enemigo 1:   _____________
   *                  |X           |
   *                 |____________|
   *      Enemigo 2:   _____________
   *                  |X           |
   *                 |____________|
   *      Enemigo 3:   _____________
   *                  |X           |
   *                 |____________|
   *    item 3. Prototype en acción
   *      Enemigo 1,2,3:   _______________
   *                      |   X   X   X  |
   *                     |______________|
   *
   */

  public static void main(String[] args) {
    System.out.println("START EG  ");
    Presentacion presentacion = new Presentacion();
    System.out.println("START Prototype");
    // Base Object, ver(item 1.)
    Enemigo enemigoBase = new Enemigo("img1.png",0,100,2);
    // Para implementar prototype, existen dos formas
    // APLICANDO LA FORMA 1
    // 1. Constructor que recibe el objeto de la misma clase, ver (item 1).
    Enemigo enemigo2 = new Enemigo(enemigoBase); // ver (Enemigo 1:)
    Enemigo enemigo1 = new Enemigo(enemigoBase); // ver (Enemigo 2:)
    Enemigo enemigo3 = new Enemigo(enemigoBase); // ver (Enemigo 3:)

    // En acción, quiero distribuir mis enemigos en la misma linea pero en diferete posición.
    // Importante, ya no es necesario poner nuevamente la imagen, ni posY, tampoco
    //              la cantidad de vida.
    enemigo1.setPosX(100); // ver (Enemigo 1,2,3:)
    enemigo2.setPosX(150); // ver (Enemigo 1,2,3:)
    enemigo3.setPosX(200); // ver (Enemigo 1,2,3:)

    // APLICANDO LA FORMA 2
    // Dibujar la seguna linea de Enemigos
    Enemigo enemigoBase2 = new Enemigo("img1.png",0,200,2);
    Enemigo enemigo4 = enemigoBase2.clone(); // ver (Enemigo 4:)
    Enemigo enemigo5 = enemigoBase2.clone(); // ver (Enemigo 5:)
    Enemigo enemigo6 = enemigoBase2.clone(); // ver (Enemigo 6:)
    // alineación, segunda linea
    enemigo4.setPosX(100); // ver (Enemigo 4,5,6:)
    enemigo5.setPosX(150); // ver (Enemigo 4,5,6:)
    enemigo6.setPosX(200); // ver (Enemigo 4,5,6:)

    presentacion.showEnemies(enemigo1);
    presentacion.showEnemies(enemigo2);
    presentacion.showEnemies(enemigo3);
    presentacion.showEnemies(enemigo4);
    presentacion.showEnemies(enemigo5);
    presentacion.showEnemies(enemigo6);

    System.out.println("END Prototype");
  }
}