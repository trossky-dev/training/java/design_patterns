package com.trossky.design_patterns.chapter03.part07_prototype;

/*****
 * Para implementar prototype, existen dos formas:
 *    Forma 1. Constructor que recibe el objeto de la misma clase:
 *        Cuando son objeto con pocos parametros, pero si son demaciado
 *        se considera la Forma 2.
 *    Forma 2. La seguna forma es crear un propio metodo de la instancia,
 *         que retorne el mismo objeto de la clase.
 */
public class Enemigo implements IEnemigo {
  private String image;
  private int posX;
  private int posY;
  private int cantidadVida;

  public Enemigo(String image, int posX, int posY, int cantidadVida) {
    this.setImage(image);
    this.setPosX(posX);
    this.setPosY(posY);
    this.setCantidadVida(cantidadVida);
  }

  // Forma 1: Constructor recibe objeto de la misma clase
  public Enemigo(Enemigo enemigo) {
    this.setImage(enemigo.getImage());
    this.setPosX(enemigo.getPosX());
    this.setPosY(enemigo.getPosY());
    this.setCantidadVida(enemigo.getCantidadVida());
  }
  // Formma 2: Usando la forma 1,
  public Enemigo clone(){
    return new Enemigo(this);
  }

  // Formma 2: Otra manera de instancia seria
  public Enemigo clone2(){
    return new Enemigo(this.image,this.posX,this.posY,this.cantidadVida);
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public int getPosX() {
    return posX;
  }

  public void setPosX(int posX) {
    this.posX = posX;
  }

  public int getPosY() {
    return posY;
  }

  public void setPosY(int posY) {
    this.posY = posY;
  }

  public int getCantidadVida() {
    return cantidadVida;
  }

  public void setCantidadVida(int cantidadVida) {
    this.cantidadVida = cantidadVida;
  }

  @Override
  public String toString() {
    return "Enemigo{" +
        "image='" + image + '\'' +
        ", posX=" + posX +
        ", posY=" + posY +
        ", cantidadVida=" + cantidadVida +
        '}';
  }
}
