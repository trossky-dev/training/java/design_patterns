package com.trossky.design_patterns.chapter03.part02_factory_method;

public class PizzaOrillaRellena extends Pizza {

  // Already We Have subtype from Pizza
  public PizzaOrillaRellena(int cantidadRebanadas, String especialidad) {
    super(cantidadRebanadas, especialidad);
  }
}
