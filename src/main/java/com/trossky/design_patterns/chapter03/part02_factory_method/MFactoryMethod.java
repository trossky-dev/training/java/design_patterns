package com.trossky.design_patterns.chapter03.part02_factory_method;


import com.trossky.design_patterns.chapter03.part01_simple_factory.Pizzeria;

/***********************************************************************************************************************
 *                                    PATRONES DE DISEÑO PARA CREACIÖN DE OBJETOS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [SF]-Simple Factory --
 *               No es considerado como un patron de diseño, pero sirve para la creación de objetos complejos.
 *               O para conrolar la creación de los objetos.
 *               Example:
 *               Ver la sig. clase {@link Pizzeria}
 *
 * -- [FM]-Factory Method --
 *                        Define an interface for creating an object,
 *                        but let subclasses decide which class to instantiate.
 *                        Factory Method lets a class defer instantiation to subclasses.
 *                        Example:
 *                        Nos permite crear objetos complejos de una forma controlada algo así como Simple Facotry,
 *                        la diferencia radica que en este patron vamos a usar una interface.
 *                        ver la sig. clase {@link IPizzeria}, {@link PizzeriaTKY} , {@link PizzaOrillaRellena}
 */
public class MFactoryMethod {
  public static void main(String[] args) {
    System.out.println("START EG  ");

    System.out.println("START FactoryMethod ");
    PizzeriaTKY pizzeriaTKY = new PizzeriaTKY();
    Pizza peperoni = pizzeriaTKY.crearPizza("Peperoni");
    Pizza hawaiana = pizzeriaTKY.crearPizza("Hawaiana");
    Pizza peperoniOrillaRellena = pizzeriaTKY.crearPizza("Peperoni orilla rellena");

    System.out.println(peperoni);
    System.out.println(hawaiana);
    System.out.println(peperoniOrillaRellena);
  }
}
