package com.trossky.design_patterns.chapter03.part02_factory_method;

public interface IPizzeria {
  Pizza crearPizza(String tipoPizza);
}
