package com.trossky.design_patterns.chapter03.part02_factory_method;

/**
 *  Aplicando Simple Factory
 */
public class Pizza {
  private int cantidadRebanadas;
  private String especialidad;

  public Pizza(int cantidadRebanadas, String especialidad) {
    this.cantidadRebanadas = cantidadRebanadas;
    this.especialidad = especialidad;
  }

  @Override
  public String toString() {
    return "Pizza{" +
        "cantidadRebanadas=" + cantidadRebanadas +
        ", especialidad='" + especialidad + '\'' +
        '}';
  }
}
