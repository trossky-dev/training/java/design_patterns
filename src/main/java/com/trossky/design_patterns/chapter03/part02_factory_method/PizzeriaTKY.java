package com.trossky.design_patterns.chapter03.part02_factory_method;

/**
 *                      Factory Method Applied
 *  PizzeriaTKY es la subclase de IPizzeria
 *    La subclase(PizzeriaTKY) es la que va a decir que tipo de instancia va a retornar
 *    Esta subclase puede retornar dos tipos de clases
 *        1.Clase {@link Pizza}
 *        2. Clase {@link PizzaOrillaRellena}
 *
 */
public class PizzeriaTKY implements IPizzeria {

  public Pizza crearPizza(String tipoPizza) {
    if (tipoPizza.equals("Peperoni")){
      return new Pizza(8, tipoPizza);
    }
    if (tipoPizza.equals("Hawaiana")){
      return new Pizza(8, tipoPizza);
    }
    if (tipoPizza.equals("Peperoni orilla rellena")){
      return new PizzaOrillaRellena(12, tipoPizza);
    }
    return null;
  }
}
