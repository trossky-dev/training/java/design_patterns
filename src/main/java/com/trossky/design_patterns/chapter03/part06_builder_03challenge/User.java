package com.trossky.design_patterns.chapter03.part06_builder_03challenge;

/**
 * --Builder --
 * -Builder- provides solution to Telescopic constructors and constructor overload.
 * -Builder- allows the creation of complex objects, in a simple way, through representations
 * <p>
 * -- IF-- the attrib {@link meansContact} have as value TRUE, So the customer
 * can assign values to email, phone and address.
 * -- IF-- the attrib {@link meansContact} have as value FALSE, So the customer
 *                           can't assign values to email, phone and address.
 *CHALLENGE
 * 1.If and only If the three attrs {@link email}, {@link phone}, {@link address} have values,
 *       SO the customer can assign value to  {@link paymentMethod}.
 * 1.If and only If the attr  {@link paymentMethod}  have a value, So the customer
 *  *    can assign value to  {@link token}.
 */
public class User {

  // required
  private  String name;
  // required
  private String lastName;

  // optionals
  private boolean meansContact;
  private String email;
  private String phone;
  private String address;
  private String paymentMethod;
  private String token;

  // 1 step: Constructor private!
  private User(final String name, final String lastName) {
    this.name = name;
    this.lastName = lastName;
    this.meansContact = false;
  }

  // 2 step: Generate a public and static method that allows us to return a new class object
  public static User make(final String name, final String lastName) {
    return new User(name, lastName);
  }

  //3. step: All setters modify it so that it returns the class object..
  // only Optional attributes have setters
  public BuilderUser setMeansContact(final boolean meansContact) {
    if (!meansContact) {
      throw new IllegalArgumentException("An empty value cannot be assigned to  MeansContact");
    }
    this.meansContact = meansContact;
    return new BuilderUser(this);
  }

  // 4 step: Method that returns the final object, the method is instance
  public User build() {
    return this;
  }

  public static class BuilderUser {
    private User user;

    public BuilderUser(final User user) {
      this.user = user;
      this.user.address = "";
      this.user.phone = "";
      this.user.address = "";
    }

    public BuilderUser setEmail(final String email) {
      user.email = email;
      return this;
    }

    public BuilderUser setPhone(final String phone) {
      user.phone = phone;
      return this;
    }

    public BuilderUser setAddress(final String address) {
      user.address = address;
      return this;
    }

    public BuilderToken setPaymentMethod(final String paymentMethod) {
      isValidConditionalToPaymentMethod();
      validatorPaymentMethod(paymentMethod);
      user.paymentMethod = paymentMethod;
      return new BuilderToken(user);
    }

    private void validatorPaymentMethod(final String paymentMethod) {
      if (paymentMethod.isEmpty()) {
        throw new IllegalArgumentException("An empty value cannot be assigned to  PaymentMethod ;)");
      }
    }

    private void isValidConditionalToPaymentMethod() {
      if (user.address.isEmpty() || user.email.isEmpty() || user.phone.isEmpty()) {
        throw new IllegalArgumentException("An empty value cannot be assigned to address, email or phone  ;)");
      }
    }

    public User build() {
      return user;
    }
  }

  public static class BuilderToken {
    private User user;
    public BuilderToken(final User user) {
      this.user = user;
    }
    public BuilderToken setToken(final String token) {
      user.token = token;
      return this;
    }
    public User build() {
      return user;
    }
  }

  @Override
  public String toString() {
    return "User{" +
        "name='" + name + '\'' +
        ", lastName='" + lastName + '\'' +
        ", meansContact=" + meansContact +
        ", email='" + email + '\'' +
        ", phone='" + phone + '\'' +
        ", address='" + address + '\'' +
        ", paymentMethod='" + paymentMethod + '\'' +
        ", token='" + token + '\'' +
        '}';
  }
}
