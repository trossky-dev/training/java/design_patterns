package com.trossky.design_patterns.chapter03.part06_builder_02part;

import com.trossky.design_patterns.chapter03.part01_simple_factory.Pizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.IPizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzaOrillaRellena;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzeriaTKY;
import com.trossky.design_patterns.chapter03.part03_abstract_factory.*;
import com.trossky.design_patterns.chapter03.part05_singleton_concurrente.ConexionDB;

/***********************************************************************************************************************
 *                                    PATRONES DE DISEÑO PARA CREACIÓN DE OBJETOS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [SF]-Simple Factory --
 *               No es considerado como un patron de diseño, pero sirve para la creación de objetos complejos.
 *               O para conrolar la creación de los objetos.
 *               Example:
 *               Ver la sig. clase {@link Pizzeria}
 *
 * -- [FM]-Factory Method --
 *                        Define an interface for creating an object,
 *                        but let subclasses decide which class to instantiate.
 *                        Factory Method lets a class defer instantiation to subclasses.
 *                        Example:
 *                        Nos permite crear objetos complejos de una forma controlada algo así como Simple Facotry,
 *                        la diferencia radica que en este patron vamos a usar una interface.
 *                        ver la sig. clase {@link IPizzeria}, {@link PizzeriaTKY} , {@link PizzaOrillaRellena}
 *
 * -- [FM]-Abstract Factory --
 *                        Provide an interface for creating families of related or dependent objects
 *                        without specifying their concrete classes.
 *                        Example:
 *                        Please see:
 *                          {@link IAbstractFactory}
 *                                -> {@link IComputadora} Definition Family
 *                                -> {@link ITablet} Definition Family
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link MacBookPro} from family {@link IComputadora}
 *                                -> {@link Ipad} from family {@link ITablet}
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link QX410} from family  {@link IComputadora}
 *                                -> {@link TabS3} from family {@link ITablet}
 *
 * -- [S]-Singleton --
 *                        Ensure a class only has one instance, and provide a global point of access to it.
 *                        Example:
 *                        La implementación de este patron es relativamente sencillo, PERO se debe validar
 *                        los errores que pueden surgir, más cuando hay temas de concurrencia.
 *
 *                        Se usa este patron siempre que necesitemos una sola instancia en nuestra applicación,
 *                        como lo es un carro de compras en nuestra aplicación, unicamente va existir un unico carro.
 *
 *                        Otro ejemplo, es el acceso a bases de datos, en caso como Android, podemos tener N cantidad
 *                        de Activities, y todas ellas requiere acceso a la base datos, entonces unicamente se crea
 *                        una conexión a base de datos.
 *                        Tambien el manejo de sessión, Cokies, entre otras.
 * -- hashCode() --
 *              Devuelve un hash unico para cada objeto.
 *
 * -- [SC]-Singleton Concurrent --
 *                        Singleton, but the Class is concurrent now, So You can use connections to DB using
 *                        thread without problem because It class now is using -- SYNCHRONIZED --
 *                        Example:
 *                        See class {@link ConexionDB}
 *
 * -- [B]-Builder --
 *                        Separate the construction of a complex object from its representation so that the same
 *                        construction process can create  different representation.
 *                        Example
 *                        See class {@link Usuario} and {@link Usuario.BuilderUsuario}
 *                        Add conditional to create a object.
 */
public class MBuilder {
  public static void main(String[] args) {
    System.out.println("START EG  ");
    System.out.println("START Build");

    Usuario tky = Usuario.Make("luis", "garcia")
        .setMedioContacto(false)
        .setEmail("luis@gamil.com").Build();

    System.out.println(tky);
    System.out.println("END Build");
  }
}