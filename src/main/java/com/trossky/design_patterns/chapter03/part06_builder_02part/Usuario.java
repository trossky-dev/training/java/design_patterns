package com.trossky.design_patterns.chapter03.part06_builder_02part;

/**
 * --Builder --
 * Builder da solución a los contructores Telecopicos y
 * a la sobrecarga de constructores.
 * Por medio de la creación de objetos complejos, de una forma bastante simple,
 * mediante representaciones
 * <p>
 * -- IF-- the attrib(medioContacto) have as value TRUE, So the customer
 * can assign values to email, phone and address.
 * -- IF-- the attrib(medioContacto) have as value FALSE, So the customer
 *                           can't assign values to email, phone and address.
 *CHALLENGE
 * 1.If and only If the three attrs(email,telefono,direccion) have values.
 *
 */

public class Usuario {

  // required
  private String nombre;
  // required
  private String apellido;
  //optional
  private boolean medioContacto;
  // optional
  private String email;
  // optional
  private String telefono;
  // optional
  private String direccion;

  // optional
  //private String metodoPago;

  // optional
  //private String token;

  // 1 step: Constructor private!
  private Usuario(String nombre, String apellido) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.medioContacto = false;
  }

  // 2 step: Generar un metodo publico y statico que nos permita retornar un nuevo objeto de la clase
  public static Usuario Make(String nombre, String apellido) {
    return new Usuario(nombre, apellido);
  }

  // Retorn a BuilderUser
  public BuilderUsuario setMedioContacto(boolean medioContacto) {
    if (!medioContacto) {
      throw new IllegalArgumentException("No es posible asignar un valor falso a medio de contacto");
    }
    this.medioContacto = medioContacto;
    return new BuilderUsuario(this);
  }

  // 4 step: Metodo que retorna el objeto final, el metodo es de instancia
  public Usuario Build() {
    return this;
  }

  @Override
  public String toString() {
    return "Usuario{" +
        "nombre='" + nombre + '\'' +
        ", apellido='" + apellido + '\'' +
        ", email='" + email + '\'' +
        ", telefono='" + telefono + '\'' +
        ", direccion='" + direccion + '\'' +
        '}';
  }

  public static class BuilderUsuario {
    private Usuario usuario;

    public BuilderUsuario(Usuario usuario) {
      this.usuario = usuario;
    }

    //3. step: A todos los setter modificarlo para que retorne el objeto de la clase.
    // Setters of attrib optionals
    // Modified setter method
    public BuilderUsuario setEmail(String email) {
      usuario.email = email;
      return this;
    }

    public BuilderUsuario setTelefono(String telefono) {
      usuario.telefono = telefono;
      return this;
    }

    public BuilderUsuario setDireccion(String direccion) {
      usuario.direccion = direccion;
      return this;
    }

    public Usuario Build() {
      return usuario;
    }
  }
}
