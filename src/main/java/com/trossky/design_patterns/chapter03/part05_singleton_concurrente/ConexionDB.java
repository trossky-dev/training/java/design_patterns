package com.trossky.design_patterns.chapter03.part05_singleton_concurrente;

/**
 * Singleton
 * But the Class is concurrent now, So You can use connections to DB using thread without problem
 * because It class now is using -- SYNCHRONIZED --
 * -- synchronized --
 *              Garantiza que solo se cree un objeto apesar de la concurrencía.
 */
public class ConexionDB {

  private static ConexionDB conexionDB;
  public String host;
  // Step 1: Private constructor
  private ConexionDB() {
  }

  public  synchronized static ConexionDB getConexionDB() {
    if (conexionDB == null) { // If object have not be instanced, so object will is instanced
      conexionDB = new ConexionDB();
    }
    return conexionDB;
  }
}
