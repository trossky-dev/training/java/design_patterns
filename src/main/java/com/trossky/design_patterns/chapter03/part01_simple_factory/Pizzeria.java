package com.trossky.design_patterns.chapter03.part01_simple_factory;

/**
 *                                      Usando Simple factory
 *
 * Una de las razones para implementar Simple factory, es si queremos controlar los atributos que tendra un objeto.
 *
 * Eg.
 *      En la pizzeria, unicamente se podran crear 3 tipos de piza:
 *        1. Chica 6 rebanadas
 *        2. Mediana 8 rebanadas
 *        3. Grande  12 rebanadas
 */
public class Pizzeria {

  public Pizza crearPizzaChica(){
    // Acá se debe consumir API, DB , File., según requiera para la creación del Objeto.
    return new Pizza(6);
  }
  public Pizza crearPizzaMediana(){
    return new Pizza(8);
  }
  public Pizza crearPizzaGrande(){
    return new Pizza(12);
  }

}
