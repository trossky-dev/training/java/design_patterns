package com.trossky.design_patterns.chapter03.part01_simple_factory;

/***********************************************************************************************************************
 *                                    PATRONES DE DISEÑO PARA CREACIÖN DE OBJETOS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [SF]-Simple Factory --
 *               No es considerado como un patron de diseño, pero sirve para la creación de objetos complejos.
 *               O para conrolar la creación de los objetos.
 *               Example:
 *               Ver la sig. clase {@link Pizzeria}
 *
 */
public class MSimpleFactory {

  public static void main(String[] args) {
    System.out.println("START EG  ");
    // Intancia SIN usar Simple Factory
    //Pizza pizzaPeperoni = new Pizza(8);
    // Intancia CON  Simple Factory
    Pizzeria pizzeriaTKY = new Pizzeria();
    System.out.println("START SimpleFactory ");
    Pizza pizzaPeperoni = pizzeriaTKY.crearPizzaChica();
    System.out.println(pizzaPeperoni);

  }
}
