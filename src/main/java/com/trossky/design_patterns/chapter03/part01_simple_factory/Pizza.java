package com.trossky.design_patterns.chapter03.part01_simple_factory;

/**
 *  Aplicando Simple Factory
 */
public class Pizza {
  private int cantidadRebanadas;

  public Pizza(int cantidadRebanadas) {
    this.cantidadRebanadas = cantidadRebanadas;
  }

  @Override
  public String toString() {
    return "pizza: {" +
        "cantidadRebanadas:" + cantidadRebanadas +
        '}';
  }
}
