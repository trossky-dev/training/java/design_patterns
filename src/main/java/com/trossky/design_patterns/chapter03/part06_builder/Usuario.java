package com.trossky.design_patterns.chapter03.part06_builder;

/**
 * --Builder --
 *          Builder da solución a los contructores Telecopicos y
 *          a la sobrecarga de constructores.
 *          Por medio de la creación de objetos complejos, de una forma bastante simple,
 *          mediante representaciones
 */

public class Usuario {

  // required
  private String nombre;
  // required
  private String apellido;

  // optional
  private String email;
  // optional
  private String telefono;
  // optional
  private String direccion;

  // 1 step: Constructor private!
  private Usuario(String nombre, String apellido) {
    this.nombre = nombre;
    this.apellido = apellido;
  }
  // 2 step: Generar un metodo publico y statico que nos permita retornar un nuevo objeto de la clase
  public static Usuario Make(String nombre, String apellido){
    return new Usuario(nombre, apellido);
  }
  //3. step: A todos los setter modificarlo para que retorne el objeto de la clase.
  // Setters of attrib optionals
  // Modified setter method
  public Usuario setEmail(String email) {
    this.email = email;
    return this;
  }

  public Usuario setTelefono(String telefono) {
    this.telefono = telefono;
    return this;
  }

  public Usuario setDireccion(String direccion) {
    this.direccion = direccion;
    return this;
  }

  // 4 step: Metodo que retorna el objeto final, el metodo es de instancia
  public Usuario Build(){
    return this;
  }

  @Override
  public String toString() {
    return "Usuario{" +
        "nombre='" + nombre + '\'' +
        ", apellido='" + apellido + '\'' +
        ", email='" + email + '\'' +
        ", telefono='" + telefono + '\'' +
        ", direccion='" + direccion + '\'' +
        '}';
  }
}
