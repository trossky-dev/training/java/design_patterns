package com.trossky.design_patterns.chapter03.part04_singleton;

/**
 * Singleton
 */
public class ConexionDB {

  private static ConexionDB conexionDB;
  public String host;
  // Step 1: Private constructor
  private ConexionDB() {
  }

  public static ConexionDB getConexionDB() {
    if (conexionDB == null) { // If object have not be instanced, so object will is instanced
      conexionDB = new ConexionDB();
    }
    return conexionDB;
  }
}
