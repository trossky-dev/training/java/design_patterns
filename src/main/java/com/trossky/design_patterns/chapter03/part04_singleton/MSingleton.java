package com.trossky.design_patterns.chapter03.part04_singleton;

import com.trossky.design_patterns.chapter03.part01_simple_factory.Pizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.IPizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzaOrillaRellena;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzeriaTKY;
import com.trossky.design_patterns.chapter03.part03_abstract_factory.*;

import static com.trossky.design_patterns.chapter03.part04_singleton.ConexionDB.getConexionDB;

/***********************************************************************************************************************
 *                                    PATRONES DE DISEÑO PARA CREACIÓN DE OBJETOS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [SF]-Simple Factory --
 *               No es considerado como un patron de diseño, pero sirve para la creación de objetos complejos.
 *               O para conrolar la creación de los objetos.
 *               Example:
 *               Ver la sig. clase {@link Pizzeria}
 *
 * -- [FM]-Factory Method --
 *                        Define an interface for creating an object,
 *                        but let subclasses decide which class to instantiate.
 *                        Factory Method lets a class defer instantiation to subclasses.
 *                        Example:
 *                        Nos permite crear objetos complejos de una forma controlada algo así como Simple Facotry,
 *                        la diferencia radica que en este patron vamos a usar una interface.
 *                        ver la sig. clase {@link IPizzeria}, {@link PizzeriaTKY} , {@link PizzaOrillaRellena}
 *
 * -- [FM]-Abstract Factory --
 *                        Provide an interface for creating families of related or dependent objects
 *                        without specifying their concrete classes.
 *                        Example:
 *                        Please see:
 *                          {@link IAbstractFactory}
 *                                -> {@link IComputadora} Definition Family
 *                                -> {@link ITablet} Definition Family
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link MacBookPro} from family {@link IComputadora}
 *                                -> {@link Ipad} from family {@link ITablet}
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link QX410} from family  {@link IComputadora}
 *                                -> {@link TabS3} from family {@link ITablet}
 *
 * -- [FM]-Singleton --
 *                        Ensure a class only has one instance, and provide a global point of access to it.
 *                        Example:
 *                        La implementación de este patron es relativamente sencillo, PERO se debe validar
 *                        los errores que pueden surgir, más cuando hay temas de concurrencia.
 *
 *                        Se usa este patron siempre que necesitemos una sola instancia en nuestra applicación,
 *                        como lo es un carro de compras en nuestra aplicación, unicamente va existir un unico carro.
 *
 *                        Otro ejemplo, es el acceso a bases de datos, en caso como Android, podemos tener N cantidad
 *                        de Activities, y todas ellas requiere acceso a la base datos, entonces unicamente se crea
 *                        una conexión a base de datos.
 *                        Tambien el manejo de sessión, Cokies, entre otras.
 * -- hashCode() --
 *              Devuelve un hash unico para cada objeto.
 */
public class MSingleton {
  public static void main(String[] args) {
    System.out.println("START EG  ");
    System.out.println("START Singleton");

    ConexionDB conexionDB = getConexionDB();
    ConexionDB conexionDB2 = getConexionDB();
    ConexionDB conexionDB3 = getConexionDB();

    // Validamos que sea el mismo objeto,
    // cada vez que llamemos una nueva conexión DB.
    System.out.println(conexionDB.hashCode());
    System.out.println(conexionDB2.hashCode());
    System.out.println(conexionDB3.hashCode());

    // Ejemplo practico de valicación.
    conexionDB.host = "localhost";
    System.out.println(conexionDB.host);
    System.out.println(conexionDB2.host);
    System.out.println(conexionDB3.host);

    System.out.println("END Singleton");

  }
}
