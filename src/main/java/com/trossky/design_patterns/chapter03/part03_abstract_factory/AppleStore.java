package com.trossky.design_patterns.chapter03.part03_abstract_factory;



/**
 * -- This class is a Factory from {@link IAbstractFactory} --
 *  -- Define type object from family defined in the  {@link IAbstractFactory} --
 *        Este factory decide que Objeto va a retornar,
 *        Ojo: el objeto debe pertenecer a la familia que se definio en {@link IAbstractFactory}
 */
public class AppleStore implements IAbstractFactory {

  public IComputadora crearComputadora() {
    return new MacBookPro();
  }

  public ITablet crearTablet() {
    return new Ipad();
  }
}
