package com.trossky.design_patterns.chapter03.part03_abstract_factory;

/**
 *                         -- Abstract Factory Applied --
 *
 *    Aca definnicmos N cantidad de metodos para crear objetos,
 *    a diferencia de Factory Method, solo teniamos un solo metodo creador.
 *
 *    Ahora podemos crear N objetos que sean de tipo {@link IComputadora} y {@link ITablet}
 *    Con la Interfaz {@link IAbstractFactory} se pueden crear N cantidad de Factories
 *
 *    Indicamos que vamos a crear objetos de la Familia {@link IComputadora} y {@link ITablet}
 *    Pero no el tipo de objeto de cada familia, bueno el tipo de Objeto lo especifica
 *    nuestros Factories como:
 *                        1. Factory {@link AppleStore}
 *                        2. Factory {@link SamsungStore}
 */
public  interface IAbstractFactory {

   IComputadora crearComputadora();
   ITablet crearTablet();
}
