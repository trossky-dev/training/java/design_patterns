package com.trossky.design_patterns.chapter03.part03_abstract_factory;

import com.trossky.design_patterns.chapter03.part01_simple_factory.Pizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.IPizzeria;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzaOrillaRellena;
import com.trossky.design_patterns.chapter03.part02_factory_method.PizzeriaTKY;

/***********************************************************************************************************************
 *                                    PATRONES DE DISEÑO PARA CREACIÓN DE OBJETOS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [SF]-Simple Factory --
 *               No es considerado como un patron de diseño, pero sirve para la creación de objetos complejos.
 *               O para conrolar la creación de los objetos.
 *               Example:
 *               Ver la sig. clase {@link Pizzeria}
 *
 * -- [FM]-Factory Method --
 *                        Define an interface for creating an object,
 *                        but let subclasses decide which class to instantiate.
 *                        Factory Method lets a class defer instantiation to subclasses.
 *                        Example:
 *                        Nos permite crear objetos complejos de una forma controlada algo así como Simple Facotry,
 *                        la diferencia radica que en este patron vamos a usar una interface.
 *                        ver la sig. clase {@link IPizzeria}, {@link PizzeriaTKY} , {@link PizzaOrillaRellena}
 *
 * -- [FM]-Abstract Factory --
 *                        Provide an interface for creating families of related or dependent objects
 *                        without specifying their concrete classes.
 *                        Example:
 *                        Please see:
 *                          {@link IAbstractFactory}
 *                                -> {@link IComputadora} Definition Family
 *                                -> {@link ITablet} Definition Family
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link MacBookPro} from family {@link IComputadora}
 *                                -> {@link Ipad} from family {@link ITablet}
 *
 *                          {@link AppleStore} factory from {@link IAbstractFactory}
 *                                -> {@link QX410} from family  {@link IComputadora}
 *                                -> {@link TabS3} from family {@link ITablet}
 */
public class MAbstractFactory {
  public static void main(String[] args) {
    System.out.println("START EG  ");

    System.out.println("START Abstract Factory");
    //       -- instantiate factories --
    SamsungStore samsungStore = new SamsungStore();
    AppleStore appleStore = new AppleStore();

    // Apple
    IComputadora mac = appleStore.crearComputadora();
    ITablet ipad = appleStore.crearTablet();

    // Samsung
    IComputadora qx = samsungStore.crearComputadora();
    ITablet s3 = samsungStore.crearTablet();

    System.out.println("END Abstract Factory successful!");

  }
}
