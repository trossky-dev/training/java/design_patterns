package com.trossky.design_patterns.chapter04.part07_flyweight;

public class Cloud {
  private TypeCloud type;
  private String img;
  private int posX;
  private int posY;

  public Cloud(TypeCloud type, String img, int posX, int posY) {
    this.type = type;
    this.img = img;
    this.posX = posX;
    this.posY = posY;
  }

  public TypeCloud getType() {
    return type;
  }

  public void setType(TypeCloud type) {
    this.type = type;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public int getPosX() {
    return posX;
  }

  public void setPosX(int posX) {
    this.posX = posX;
  }

  public int getPosY() {
    return posY;
  }

  public void setPosY(int posY) {
    this.posY = posY;
  }
}
