package com.trossky.design_patterns.chapter04.part07_flyweight;


public enum TypeCloud {
  SMALL,
  MEDIUM,
  BIG
}
