package com.trossky.design_patterns.chapter04.part07_flyweight;

import java.util.HashMap;

public class CloudFactory {
  HashMap<TypeCloud, Cloud> cloudMap;

  public CloudFactory() {
    this.cloudMap = new HashMap<>();
  }

  public Cloud getCloud(TypeCloud type) {
    Cloud cloud = (Cloud)this.cloudMap.get(type);
    if (cloud == null) {
      cloud = new Cloud(type, "nube.png", 100, 100);
      cloudMap.put(type, cloud);
    }
    return cloud;
  }
  public int size(){
    return this.cloudMap.size();
  }
}
