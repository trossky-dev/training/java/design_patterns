package com.trossky.design_patterns.chapter04.part02_composite;

import java.util.ArrayList;

public class Menu implements IMenu {
  private ArrayList<IMenu> menus;

  public Menu() {
    this.menus= new ArrayList<IMenu>();
  }
  public boolean open() {
    System.out.println("Open!");
    return true;
  }

  public boolean close() {
    System.out.println("Close!");
    return true;
  }

  public void addMenus(IMenu menus) {
    this.menus.add(menus);
  }
  public IMenu getMenu(int pos) {
    return this.menus.get(pos);
  }
}
