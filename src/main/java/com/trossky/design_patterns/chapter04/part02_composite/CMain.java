package com.trossky.design_patterns.chapter04.part02_composite;

import com.trossky.design_patterns.chapter04.part01_adapter.AdapterDB;

/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR STRUCTURE OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [A]-Adapter --
 *               Convert the interface of a class into another interface clients expect.
 *               Adapter lets classes works together that cloud not otherwise because of
 *               incompatible interfaces.
 *               Example:
 *               Permite extender la applicación sin modificar la app.
 *               See class {@link AdapterDB}
 *
 * -- [C]-Composite --
 *               Compose objects into tree structures to represent part-whole hierarchies.
 *               Composite lets clients treat individual objects and compositions
 *               of objects uniformly.
 *               Example:
 *               See class {@link Menu}
 *
 */
public class CMain {
  public static void main(String[] args) {
    System.out.println("START EG  ");
    System.out.println("START Composite");

    Menu menu = new Menu();

    Menu menu2 = new Menu();
    Menu menu3 = new Menu();

    Menu menu4 = new Menu();
    Menu menu5 = new Menu();

    menu3.addMenus(menu5);
    menu3.addMenus(menu4);

    menu.addMenus(menu3);
    menu.addMenus(menu2);

    menu3.getMenu(1).open();

    menu.open();
    menu.close();

    System.out.println("result");
    System.out.println("END Composite");
  }
}
