package com.trossky.design_patterns.chapter04.part02_composite;

public interface IMenu {
  boolean open();
  boolean close();
}
