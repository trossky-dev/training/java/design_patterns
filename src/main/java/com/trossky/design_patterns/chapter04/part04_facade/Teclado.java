package com.trossky.design_patterns.chapter04.part04_facade;

/**
 * This class is low level module
 */
public class Teclado implements ITeclado {

  public void conectar(){
    System.out.println("Conexión Teclado vía USB!");
  }
}
