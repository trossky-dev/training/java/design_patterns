package com.trossky.design_patterns.chapter04.part04_facade;

/**
 * This class is low level module
 */
public class Mouse implements IMouse {

  public void conectar(){
    System.out.println("Conexión Mouse vía USB!");
  }
}
