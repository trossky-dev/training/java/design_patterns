package com.trossky.design_patterns.chapter04.part04_facade;

/**
 * This class is high level module
 */
public class Computadora {


  //Tenemos un fuerte acoplamiento acá
  //Este es un ejemplo de -- CONCREACIONES --
  // private Mouse mouse;
  // private Teclado teclado;

  // Este es un ejemplo de -- ABSTRACIONES --
  private IMouse mouse;
  private ITeclado teclado;

  // De esta forma la clase Computadora, podrá aceptar cualquier tipo de Teclado, Alambrico, Inalambrico.
  // Al igual con el Mouse.
  public Computadora(IMouse mouse, ITeclado teclado) {
    this.mouse = mouse;
    this.teclado = teclado;
  }
  public void encender() {
    this.teclado.conectar();
    this.mouse.conectar();
  }

}
