package com.trossky.design_patterns.chapter04.part04_facade;


import com.trossky.design_patterns.chapter04.part01_adapter.AdapterDB;
import com.trossky.design_patterns.chapter04.part02_composite.Menu;
import com.trossky.design_patterns.chapter04.part03_decorator.OrillaRellena;
import com.trossky.design_patterns.chapter04.part03_decorator.QuesoExtra;

/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR STRUCTURE OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [A]-Adapter --
 *               Convert the interface of a class into another interface clients expect.
 *               Adapter lets classes works together that cloud not otherwise because of
 *               incompatible interfaces.
 *               Example:
 *               Permite extender la applicación sin modificar la app.
 *               See class {@link AdapterDB}
 *
 * -- [C]-Composite --
 *               Compose objects into tree structures to represent part-whole hierarchies.
 *               Composite lets clients treat individual objects and compositions
 *               of objects uniformly.
 *               Example:
 *               See class {@link Menu}
 *
 * -- [D]-Decorator --
 *               Attach additional responsibilities  to an object dynamically.
 *               Decorators provide a flexible alternative to Subclassing for
 *               extending functionally.
 *               Example:
 *               See class {@link QuesoExtra} {@link OrillaRellena}
 * -- [F]-Facade --
 *               Provide a unified interface to a set of interfaces in a subsystem.
 *               Facade defines a higher-level interface that makes the subsystem
 *               easier to use.
 *               Example:
 *               See class {@link Fachada}
 */
public class FMain {
  public static void main(String[] args) {
    System.out.println("START EG ");
    System.out.println("START Facade");

    Fachada fachaComputador = new Fachada();
    fachaComputador.encender();

    System.out.println("result");
    System.out.println("END Facade");
  }
}
