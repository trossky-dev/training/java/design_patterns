package com.trossky.design_patterns.chapter04.part04_facade;

public class Fachada {
  private Computadora computadora;

  public Fachada() {
    ITeclado teclado = new Teclado();
    IMouse mouse = new Mouse();
    this.computadora = new Computadora(mouse,teclado);
  }
  public void encender() {
    // Operaciones Complejas, Consumir API, DB, File etc.
    this.computadora.encender();
  }
}
