package com.trossky.design_patterns.chapter04.part05_proxy;

public class ProxyService implements IService {

  private IService service;
  private User user;

  public ProxyService(IService service, User user) {
    this.service = service;
    this.user = user;
  }

  @Override
  public int insert() {
    if (this.user.getPermission_level()>=5) {
      this.service.insert();
      return 1;
    } else {
      throw new UnsupportedOperationException("Error de Seguridad!");
    }
  }

  // Everyone can read
  @Override
  public String read() {

    return this.service.read();
  }

  @Override
  public int update() {
    if (this.user.getPermission_level()>=5) {
      this.service.insert();
      return 1;
    } else {
      throw new UnsupportedOperationException("Error de Seguridad!");
    }
  }

  @Override
  public int delete() {
    if (this.user.getPermission_level()>=5) {
      this.service.insert();
      return 1;
    } else {
      throw new UnsupportedOperationException("Error de Seguridad!");
    }
  }
}
