package com.trossky.design_patterns.chapter04.part05_proxy;


import com.trossky.design_patterns.chapter04.part01_adapter.AdapterDB;
import com.trossky.design_patterns.chapter04.part02_composite.Menu;
import com.trossky.design_patterns.chapter04.part03_decorator.OrillaRellena;
import com.trossky.design_patterns.chapter04.part03_decorator.QuesoExtra;
import com.trossky.design_patterns.chapter04.part04_facade.Fachada;

/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR STRUCTURE OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [A]-Adapter --
 *               Convert the interface of a class into another interface clients expect.
 *               Adapter lets classes works together that cloud not otherwise because of
 *               incompatible interfaces.
 *               Example:
 *               Permite extender la applicación sin modificar la app.
 *               See class {@link AdapterDB}
 *
 * -- [C]-Composite --
 *               Compose objects into tree structures to represent part-whole hierarchies.
 *               Composite lets clients treat individual objects and compositions
 *               of objects uniformly.
 *               Example:
 *               See class {@link Menu}
 *
 * -- [D]-Decorator --
 *               Attach additional responsibilities  to an object dynamically.
 *               Decorators provide a flexible alternative to Subclassing for
 *               extending functionally.
 *               Example:
 *               See class {@link QuesoExtra} {@link OrillaRellena}
 * -- [F]-Facade --
 *               Provide a unified interface to a set of interfaces in a subsystem.
 *               Facade defines a higher-level interface that makes the subsystem
 *               easier to use.
 *               Example:
 *               See class {@link Fachada}
 *
 * -- [P]-Proxy --
 *               Provide a surrogate or placeholder for another object to
 *               control access to it.
 *               Example:
 *               Te permite limitar funcionalidades de la app,
 *               por seguridad debemos limitar el acceso a ciertas partes
 *               del sistema
 *               See class {@link ProxyService}
 */
public class PMain {
  public static void main(String[] args) {
    System.out.println("START EG ");
    System.out.println("START Proxy");
    /*
     * -Use cases-
     *  Role Admin:
     *  The Admin can execute all operations as, insert,read,update,delete
     *  Role others:
     *    Only can execute the read operation, So You mush do proxy implement
     */
    IService service = new Service();
    User user = new User(4);
    IService pService= new ProxyService(service,user);

    pService.read();
    pService.insert();
    pService.update();
    pService.delete();


    System.out.println("END Proxy");
  }
}
