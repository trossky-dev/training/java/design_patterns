package com.trossky.design_patterns.chapter04.part06_virtual_proxy;

public class Service implements IService {
  @Override
  public int insert() {
    System.out.println("Insert register");
    return 0;
  }

  @Override
  public String read() {
    System.out.println("Read register");
    return "read register(s)";
  }

  @Override
  public int update() {
    System.out.println("Update register");
    return 0;
  }

  @Override
  public int delete() {
    System.out.println("Delete register");
    return 0;
  }
}
