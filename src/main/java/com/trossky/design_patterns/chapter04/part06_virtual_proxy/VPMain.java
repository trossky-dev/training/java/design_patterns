package com.trossky.design_patterns.chapter04.part06_virtual_proxy;


import com.trossky.design_patterns.chapter04.part01_adapter.AdapterDB;
import com.trossky.design_patterns.chapter04.part02_composite.Menu;
import com.trossky.design_patterns.chapter04.part03_decorator.OrillaRellena;
import com.trossky.design_patterns.chapter04.part03_decorator.QuesoExtra;
import com.trossky.design_patterns.chapter04.part04_facade.Fachada;


/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR STRUCTURE OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [A]-Adapter --
 *               Convert the interface of a class into another interface clients expect.
 *               Adapter lets classes works together that cloud not otherwise because of
 *               incompatible interfaces.
 *               Example:
 *               Permite extender la applicación sin modificar la app.
 *               See class {@link AdapterDB}
 *
 * -- [C]-Composite --
 *               Compose objects into tree structures to represent part-whole hierarchies.
 *               Composite lets clients treat individual objects and compositions
 *               of objects uniformly.
 *               Example:
 *               See class {@link Menu}
 *
 * -- [D]-Decorator --
 *               Attach additional responsibilities  to an object dynamically.
 *               Decorators provide a flexible alternative to Subclassing for
 *               extending functionally.
 *               Example:
 *               See class {@link QuesoExtra} {@link OrillaRellena}
 * -- [F]-Facade --
 *               Provide a unified interface to a set of interfaces in a subsystem.
 *               Facade defines a higher-level interface that makes the subsystem
 *               easier to use.
 *               Example:
 *               See class {@link Fachada}
 *
 * -- [P]-Proxy --
 *               Provide a surrogate or placeholder for another object to
 *               control access to it.
 *               Example:
 *               Te permite limitar funcionalidades de la app,
 *               por seguridad debemos limitar el acceso a ciertas partes
 *               del sistema
 *               See class {@link ProxyService}
 * -- [VP]-Virtual Proxy --
 *               In place of a complex or heavy object, a skeleton representation may be advantageous in some cases.
 *               When an underlying image is huge in size, it may be represented using a virtual proxy object,
 *               loading the real object on demand {https://en.wikipedia.org/wiki/Proxy_pattern}.
 *               Example:
 *               No restringe el acceso al objeto real, busca retrazar la creación
 *               del objeto, suele aplicarse en casos donde la creación del objeto
 *               tiene un alto costo computacional.
 *               Cuando un Objeto se crea apartir de una consulta a (BD,API o File).
 *               Entonces busca crear el objeto unicamente cuando se necesite.
 *               See class {@link }
 */
public class VPMain {
  public static void main(String[] args) {
    System.out.println("START EG ");
    System.out.println("START Virtual Proxy");


    User user = new User(5);
    IService pService= new ProxyService(user);

    // only here the program have create the object INService, But It does not happen before.
    pService.read(); //  any role
    pService.insert(); // only admin
    pService.update(); // only admin
    pService.delete(); // only admin
   

    System.out.println("END Virtual Proxy");
  }
}
