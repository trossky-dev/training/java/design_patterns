package com.trossky.design_patterns.chapter04.part06_virtual_proxy;

public class User {
  /**
   * Levels
   *    5: Admin
   *    1...4: others
   */
  private int permission_level; // Replace by Enum.

  public User(int permission_level) {
    this.setPermission_level(permission_level);
  }

  public int getPermission_level() {
    return permission_level;
  }

  public void setPermission_level(int permission_level) {
    this.permission_level = permission_level;
  }
}
