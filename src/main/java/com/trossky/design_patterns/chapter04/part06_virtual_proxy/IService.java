package com.trossky.design_patterns.chapter04.part06_virtual_proxy;

public interface IService {
  int insert();
  String read();
  int update();
  int delete();
}
