package com.trossky.design_patterns.chapter04.part06_virtual_proxy;

/**
 * Apply Vrtual Proxy
 * Show changes in getService() method
 *
 * Se esta creando una unica vez el objeto IService,
 * Y se crea cuando es necesario.
 */
public class ProxyService implements IService {

  private IService service; // Object with high cost computationally
  private User user;

  public ProxyService( User user) {
    this.user = user;
  }

  @Override
  public int insert() {
    if (this.user.getPermission_level()>=5) {
      this.getService().insert();
      return 1;
    } else {
      throw new UnsupportedOperationException("Error de Seguridad!");
    }
  }

  // Everyone can read
  @Override
  public String read() {

    return this.getService().read();
  }

  @Override
  public int update() {
    if (this.user.getPermission_level()>=5) {
      this.getService().insert();
      return 1;
    } else {
      throw new UnsupportedOperationException("Error de Seguridad!");
    }
  }

  @Override
  public int delete() {
    if (this.user.getPermission_level()>=5) {
      this.getService().insert();
      return 1;
    } else {
      throw new UnsupportedOperationException("Error de Seguridad!");
    }
  }

  private IService getService(){
    if (this.service == null) {
      this.service= new Service();
    }
    return this.service;
  }
}
