package com.trossky.design_patterns.chapter04.part01_adapter;

/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR STRUCTURE OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [A]-Adapter --
 *               Convert the interface of a class into another interface clients expect.
 *               Adapter lets classes works together that cloud not otherwise because of
 *               incompatible interfaces.
 *               Example:
 *               Permite extender la applicación sin modificar la app.
 *               See class  {@link AdapterDB}
 *
 *
 */
public class AMain {
  public static void main(String[] args) {
    IConexionSQL conexion = new AdapterDB(new ConexionMongoDB());
    conexion.conexion();
    String result = conexion.runQuery();

    System.out.println(result);

  }
}
