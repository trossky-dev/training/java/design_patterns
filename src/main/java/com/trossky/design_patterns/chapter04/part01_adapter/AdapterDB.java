package com.trossky.design_patterns.chapter04.part01_adapter;

public class AdapterDB implements IConexionSQL {
  private IConexionNoSQL conexionNoSQL;

  public AdapterDB(IConexionNoSQL conexionNoSQL) {
    this.conexionNoSQL = conexionNoSQL;
  }

  @Override
  public void conexion() {
    this.conexionNoSQL.conexion();
  }

  @Override
  public String runQuery() {
    return this.conexionNoSQL.executeSentense();
  }
}
