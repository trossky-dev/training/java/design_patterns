package com.trossky.design_patterns.chapter04.part01_adapter;

public class ConexionMySQL implements IConexionSQL {
  @Override
  public void conexion() {
    System.out.println("Coneción establecida MySQL");
  }

  @Override
  public String runQuery() {
    return "Inicia la consulta MySQL";
  }
}
