package com.trossky.design_patterns.chapter04.part01_adapter;

public interface IConexionNoSQL {
  void conexion();
  String executeSentense();
}
