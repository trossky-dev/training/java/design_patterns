package com.trossky.design_patterns.chapter04.part01_adapter;

public interface  IConexionSQL {
   void conexion();
  String runQuery();
}
