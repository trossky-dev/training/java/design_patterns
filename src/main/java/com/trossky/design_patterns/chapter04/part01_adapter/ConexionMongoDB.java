package com.trossky.design_patterns.chapter04.part01_adapter;

public class ConexionMongoDB implements IConexionNoSQL {

  @Override
  public void conexion() {
    System.out.println("Conección establecida MongoDB");
  }

  @Override
  public String executeSentense() {
    return "Inicia la consulta NoSLQ MongoDB";
  }
}
