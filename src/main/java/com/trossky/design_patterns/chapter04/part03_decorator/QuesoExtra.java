package com.trossky.design_patterns.chapter04.part03_decorator;

/**
 * Aplicando Decorator
 */
public class QuesoExtra  implements IPizza{

  // Compocisiòn
  private IPizza pizza;

  public QuesoExtra(IPizza pizza) {
    this.pizza = pizza;
  }

  @Override
  public String description() {
    return this.pizza.description() + " Queso extra";
  }

  @Override
  public float price() {
    return this.pizza.price()+2;
  }
}
