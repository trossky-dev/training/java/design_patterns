package com.trossky.design_patterns.chapter04.part03_decorator;

public class PizzaHawaiana implements IPizza {
  @Override
  public String description() {
    return "Pizza Hawaiana";
  }

  @Override
  public float price() {
    return 8;
  }
}
