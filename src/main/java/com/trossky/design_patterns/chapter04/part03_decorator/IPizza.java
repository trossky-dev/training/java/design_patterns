package com.trossky.design_patterns.chapter04.part03_decorator;

public interface IPizza {
  String description();
  float price();

}
