package com.trossky.design_patterns.chapter04.part03_decorator;

import com.trossky.design_patterns.chapter04.part01_adapter.AdapterDB;
import com.trossky.design_patterns.chapter04.part02_composite.Menu;

/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR STRUCTURE OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [A]-Adapter --
 *               Convert the interface of a class into another interface clients expect.
 *               Adapter lets classes works together that cloud not otherwise because of
 *               incompatible interfaces.
 *               Example:
 *               Permite extender la applicación sin modificar la app.
 *               See class {@link AdapterDB}
 *
 * -- [C]-Composite --
 *               Compose objects into tree structures to represent part-whole hierarchies.
 *               Composite lets clients treat individual objects and compositions
 *               of objects uniformly.
 *               Example:
 *               See class {@link Menu}
 *
 * -- [D]-Decorator --
 *               Attach additional responsibilities  to an object dynamically.
 *               Decorators provide a flexible alternative to Subclassing for
 *               extending functionally.
 *               Example:
 *               See class {@link QuesoExtra} {@link OrillaRellena}

 */
public class DMain {
  public static void main(String[] args) {
    System.out.println("START EG  ");

    IPizza pizzaPeperoni = new PizzaPeperoni();
    System.out.println(pizzaPeperoni.description());
    System.out.println(pizzaPeperoni.price());

    IPizza pizzaHawaiana = new PizzaHawaiana();
    System.out.println(pizzaHawaiana.description());
    System.out.println(pizzaHawaiana.price());

    /**
     * ************************************************
     *              NUEVAS REGLAS DE NEGOCIO.
     *
     * Ahora si la pizzeria permite que el cliente:
     * Regla 1. Agregue Queso Extra a la Pizza, y tendra un valor de 2 adicional
     * Regla 2. Las pizzas pueden ir o NO con orilla rellana de Queso, SI tiene orilla incrementa el precio en 4  Dolares.
     *
     * PARA ESTE CASO APLICAMOS EL PATRON DE DISEÑO DECORATOR.
     */

    // Begin Using Decorator
    System.out.println("START Decorator");
    // Regla 1.
    IPizza pizzaPeperoni2 = new QuesoExtra(new PizzaPeperoni());
    System.out.println(pizzaPeperoni2.description());
    System.out.println(pizzaPeperoni2.price());

    // Regla 2.
    IPizza pizzaHawaiana2 = new QuesoExtra(new OrillaRellena(new PizzaHawaiana()));

    System.out.println(pizzaHawaiana2.description());
    System.out.println(pizzaHawaiana2.price());

    // Composite
    PizzaComposite pizzaHawaianaComposite = new PizzaComposite();
    pizzaHawaianaComposite.addPizza(new PizzaHawaiana());
    pizzaHawaianaComposite.addPizza(new QuesoExtra(pizzaHawaianaComposite.get(0)));
    pizzaHawaianaComposite.addPizza(new OrillaRellena(pizzaHawaianaComposite.get(1)));
    System.out.println(pizzaHawaianaComposite.get(2).description());


    System.out.println("result");
    System.out.println("END Decorator");
  }
}
