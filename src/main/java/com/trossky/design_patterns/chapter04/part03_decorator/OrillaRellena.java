package com.trossky.design_patterns.chapter04.part03_decorator;

/**
 * Aplicando Decorator
 */
public class OrillaRellena  implements IPizza{

  // Compocisiòn
  private IPizza pizza;

  public OrillaRellena(IPizza pizza) {
    this.pizza = pizza;
  }

  @Override
  public String description() {
    return this.pizza.description() + " Con Orilla Rellena";
  }

  @Override
  public float price() {
    return this.pizza.price()+4;
  }
}

