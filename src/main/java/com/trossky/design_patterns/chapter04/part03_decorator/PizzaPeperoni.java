package com.trossky.design_patterns.chapter04.part03_decorator;

public class PizzaPeperoni implements IPizza {
  @Override
  public String description() {
    return "Pizza Peperoni";
  }

  @Override
  public float price() {
    return 8;
  }
}
