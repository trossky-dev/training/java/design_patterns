package com.trossky.design_patterns.chapter04.part03_decorator;

import java.util.ArrayList;

public class PizzaComposite implements IPizza {

  private ArrayList<IPizza> pizzaComposite;

  public PizzaComposite() {
    this.pizzaComposite = new ArrayList<>();
  }

  @Override
  public String description() {
    return "";
  }

  @Override
  public float price() {
    return 0;
  }

  public void addPizza(IPizza pizza) {
    pizzaComposite.add(pizza);
  }

  public IPizza get(int i) {
    return pizzaComposite.get(i);
  }

}

