package com.trossky.design_patterns.chapter05.part05_memento;

/**
 * This class have memento
 * For these, you must generate a method new to get a security copy
 */
public class User {
    private String name;
    private int age;

    public User(String name, int age) {
        this.setName(name);
        this.setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * This copy can be -TOTAL- OR -PARTIAL-
     * for this case is total copy
     * @return
     */
    public User getMemento(){
        return new User(this.getName(), this.getAge());
    }

    public void restartMemento(User userCopy) {
        this.setName(userCopy.getName());
        this.setAge(userCopy.getAge());
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
