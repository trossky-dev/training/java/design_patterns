package com.trossky.design_patterns.chapter05.part05_memento;


import com.trossky.design_patterns.chapter05.part04_mediator.IUserChat;
import com.trossky.design_patterns.chapter05.part04_mediator.RoomChat;
import com.trossky.design_patterns.chapter05.part02_command.ICommand;
import com.trossky.design_patterns.chapter05.part01_chain_responsibility.ITransactionHandler;

/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR BEHAVIORAL OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [CR]-Chain Responsibility --
 *               Avoid coupling the sender of a request ti its receiver by giving more than one object a
 *               change to handle the resquest.
 *               Chainthe receiving objects and pass the resquest along the chain until an object handles it.
 *               Example:
 *               Permite establecer una cadena de objetos receptores, la cual debe ser capaz de recibir
 *               información de un objeto emisor.
 *               Es decir el Cliente envia la petición al objeto número 1.
 *                  Sí el objeto número 1, no es capaz de responder a la petición, entonces delegara la tarea
 *                  al objeto número 2.
 *                  Ahora el Objeto número 2 intentara responder de forma correcta, pero si falla entonces delegara
 *                  al Objeto número 3. Y asi sucesivamente se delegara la responsabilidad hasta que algún objeto
 *                  logre responder satisfactoriamente.TR
 *               See class {@link ITransactionHandler}
 *
 * -- [C]-Command --
 *               Encapsulate a request as an object, thereby letting parameterize clients with different requests,
 *               queue or log requests, and support undoable operations.
 *               Example:
 *               Desacoplar al objeto que invoca la operaciòn.
 *               See class {@link ICommand}
 *
 * -- [I]-Iterator --
 *               Provide a way to access the elements of an aggregate object sequentially without exposing
 *               its underlying representation.
 *               Example:
 *               Para no exponer los atributos y metodos de la colección.
 *               See class {@link }
 *
 * -- [M]-Mediator --
 *               Define an object that encapsulates hw a set of objects interact.
 *               Mediator promotes loose coupling by keeping objects from referring
 *               to each explicity, and it lets you vary their interaction independently.
 *               Example:
 *               Una torre de control en un aeropuerto, la torre de control es el mediador para
 *               que los aviones essten en comunicación
 *               See class {@link RoomChat}
 *
 * -- [ME]-Memento --
 *               Without violating encapsulation, capture and externalize an object's internal state
 *               so that the object can be restored to this state later.
 *               Example:
 *               Se puede implementar el Ctl+z en nuestra app. Le permite crear una copia de seguridad
 *               completa o parcial del objeto para posteriormente poder revertir cambios.
 *
 *               See class {@link User}
 */
public class MEMain {

    /**
     *
     */
    public static void main(String[] args) {
        System.out.println("START EG ");
        System.out.println("START Memento");

        User user = new User("Luis", 12);
        System.out.println("Original -> "+ user);

        User userCopy = user.getMemento();
        user.setName("Mathias");
        user.setAge(4);
        System.out.println("Modified -> "+ user);

        user.restartMemento(userCopy);
        System.out.println("Ctrl+Z -> "+ user);


        System.out.println("END Memento");
    }

}
















