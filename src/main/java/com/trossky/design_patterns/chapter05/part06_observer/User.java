package com.trossky.design_patterns.chapter05.part06_observer;

/**
 * This object is a observer
 */
public class User implements IObserver {

    @Override
    public void notification(String msn) {
        System.out.println(msn);
    }
}
