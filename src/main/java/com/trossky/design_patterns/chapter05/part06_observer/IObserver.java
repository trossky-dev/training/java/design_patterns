package com.trossky.design_patterns.chapter05.part06_observer;

/**
 * the method that observer others objects observers
 * All class that implement this interface, They must be observers
 */
public interface IObserver {
    void notification(String msn);
}
