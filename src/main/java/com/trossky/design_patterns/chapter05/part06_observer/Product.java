package com.trossky.design_patterns.chapter05.part06_observer;

import java.util.LinkedHashSet;
import java.util.Set;

public class Product  implements IObservable{

    private int stock;
    private Set<IObserver> observers;

    public Product(int stock) {
        this.setStock(stock);
        // Use LinkedHashSet to notify in order all observer
        this.observers= new LinkedHashSet<>();
    }

    public void venta() {
        this.setStock(this.getStock()-1);
        System.out.println("Sale a product !");
        notificationObservers();
    }
    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * Tthis method received many observers
     * @param o
     */
    @Override
    public void addObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void notificationObservers() {
        observers.forEach(iObserver -> {
            iObserver.notification("The sale product  !!");
        });
    }

    @Override
    public void removeObserver() {

    }
}
