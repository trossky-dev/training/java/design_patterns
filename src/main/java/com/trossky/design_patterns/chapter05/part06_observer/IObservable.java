package com.trossky.design_patterns.chapter05.part06_observer;


/**
 * This interface manage the observers,
 * This interface permit to an object notify the change
 */
public interface IObservable {
    void addObserver(IObserver  o);

    void notificationObservers();

    void removeObserver();
}
