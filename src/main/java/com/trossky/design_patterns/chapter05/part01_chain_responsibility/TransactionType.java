package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

public enum TransactionType {
    DEPOSIT,
    WITHDRAWAL,
    REFUND

}
