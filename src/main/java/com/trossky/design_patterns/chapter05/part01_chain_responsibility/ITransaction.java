package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

public interface ITransaction {
    TransactionType getTransactionType();
    float getQuantity();
    float getBalance();
}
