package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR BEHAVIORAL OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [CR]-Chain Responsibility --
 *               Avoid coupling the sender of a request ti its receiver by giving more than one object a
 *               change to handle the resquest.
 *               Chainthe receiving objects and pass the resquest along the chain until an object handles it.
 *               Example:
 *               Permite establecer una cadena de objetos receptores, la cual debe ser capaz de recibir
 *               información de un objeto emisor.
 *               Es decir el Cliente envia la petición al objeto número 1.
 *                  Sí el objeto número 1, no es capaz de responder a la petición, entonces delegara la tarea
 *                  al objeto número 2.
 *                  Ahora el Objeto número 2 intentara responder de forma correcta, pero si falla entonces delegara
 *                  al Objeto número 3. Y asi sucesivamente se delegara la responsabilidad hasta que algún objeto
 *                  logre responder satisfactoriamente.TR
 *               See class {@link ITransactionHandler}
 */
public class CRMain {

    /**
     * Challenge you must applied virtual proxy, So You don't create amount objects in this class
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("START EG ");
        System.out.println("START Chain Responsibility");
        // Instances
        ITransaction transaction = new Transaction(100, 200, TransactionType.REFUND);
        // Applied virtual proxy
        TransactionProxy transactionProxy = new TransactionProxy(transaction);
        transactionProxy.transactionExecute();
        System.out.println("END Chain Responsibility");
    }

}
