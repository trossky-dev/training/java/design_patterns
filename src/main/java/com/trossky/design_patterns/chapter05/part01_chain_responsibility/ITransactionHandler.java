package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

/**
 * Aca se encadenan los objetos
 * Cadena e responsabilidades
 */
public interface ITransactionHandler {
    void setNextHandler(ITransactionHandler next);
    void transactionExecute(ITransaction transaction);

}
