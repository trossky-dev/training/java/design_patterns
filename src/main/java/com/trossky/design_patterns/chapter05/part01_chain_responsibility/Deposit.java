package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

public class Deposit implements ITransactionHandler {
    private ITransactionHandler next;
    @Override
    public void setNextHandler(ITransactionHandler next) {
        this.next = next;
    }

    @Override
    public void transactionExecute(ITransaction transaction) {
        if (transaction.getTransactionType() == TransactionType.DEPOSIT) {
            float quantity = transaction.getQuantity() + transaction.getBalance();
            System.out.println("New balance after of a deposit is: " + quantity);
        } else {
            this.next.transactionExecute(transaction );
        }
    }
}