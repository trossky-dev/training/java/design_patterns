package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

public class Refund implements ITransactionHandler {
    private ITransactionHandler next;
    @Override
    public void setNextHandler(ITransactionHandler next) {
        this.next = next;
    }

    @Override
    public void transactionExecute(ITransaction transaction) {
        if (transaction.getTransactionType() == TransactionType.REFUND) {
            float quantity = transaction.getQuantity() + transaction.getBalance();
            System.out.println("New balance after of a refund is: " + quantity);
        } else {
            System.out.println("The operation is not valid");
        }
    }
}