package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

public class Withdrawal implements ITransactionHandler {
    private ITransactionHandler next;
    @Override
    public void setNextHandler(ITransactionHandler next) {
        this.next = next;
    }

    @Override
    public void transactionExecute(ITransaction transaction) {
        if (transaction.getTransactionType() == TransactionType.WITHDRAWAL) {
            float quantity = transaction.getBalance()- transaction.getQuantity() ;
            System.out.println("New balance after of a withdrawal is: " + quantity);
        } else {
            this.next.transactionExecute(transaction );
        }
    }
}
