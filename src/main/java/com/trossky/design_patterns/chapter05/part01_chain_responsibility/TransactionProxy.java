package com.trossky.design_patterns.chapter05.part01_chain_responsibility;

/**
 * Applied virtual proxy for use case Transaction
 */
public class TransactionProxy {
    private ITransaction transaction;
    private ITransactionHandler depositHandler;
    private ITransactionHandler withdrawalHandler;
    private ITransactionHandler refundHandler;

    public TransactionProxy(ITransaction transaction) {
        this.transaction = transaction;
    }

    public void  transactionExecute() {
        // define order as step by step
        this.getDepositHandler().setNextHandler(this.getWithdrawalHandler());
        this.getWithdrawalHandler().setNextHandler(this.getRefundHandler());
        // execute chain responsibility
        this.getDepositHandler().transactionExecute(this.transaction);
    }
    private ITransactionHandler getDepositHandler() {
        if (depositHandler == null) {
            depositHandler = new Deposit();
        }
       return depositHandler;
    }
    private ITransactionHandler getWithdrawalHandler() {
        if (withdrawalHandler == null) {
            this.withdrawalHandler = new Withdrawal();
        }
        return withdrawalHandler;
    }
    private ITransactionHandler getRefundHandler() {
        if (this.refundHandler == null) {
            refundHandler = new Refund();
        }
        return refundHandler;
    }

}
