package com.trossky.design_patterns.chapter05.part02_command;

public interface ICommand {
    void operation();
}
