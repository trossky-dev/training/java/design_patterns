package com.trossky.design_patterns.chapter05.part02_command;

public class OffDevice implements ICommand {
    private IDevice device;

    public OffDevice(IDevice device) {
        this.device = device;
    }

    @Override
    public void operation() {
        this.device.off();
    }
}
