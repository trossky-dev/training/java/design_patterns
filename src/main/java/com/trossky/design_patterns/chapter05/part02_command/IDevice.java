package com.trossky.design_patterns.chapter05.part02_command;

public interface IDevice {
    void on();
    void off();
}
