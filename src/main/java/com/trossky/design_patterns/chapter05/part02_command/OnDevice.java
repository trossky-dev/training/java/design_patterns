package com.trossky.design_patterns.chapter05.part02_command;

public class OnDevice implements ICommand {
    private IDevice device;

    public OnDevice(IDevice device) {
        this.device = device;
    }

    @Override
    public void operation() {
        this.device.on();
    }
}
