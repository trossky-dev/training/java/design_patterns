package com.trossky.design_patterns.chapter05.part04_mediator;

import java.util.HashMap;

/**
 * Mediator
 */
public class RoomChat {
    private HashMap<String, IUserChat> users;

    public RoomChat() {
        this.users = new HashMap<>();
    }
    public void addMember(IUserChat user) {
        users.put(user.getName(), user);
    }

    public void sendMessage(IUserChat userFrom, IUserChat userTo, String message ) {
        if (users.get(userFrom.getName()) != null
                && users.get(userTo.getName()) != null) {

            message = "From :" + userFrom.getName() + " message :" + message;
            userTo.receiveMsn(message);
        }
    }
}
