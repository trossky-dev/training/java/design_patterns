package com.trossky.design_patterns.chapter05.part04_mediator;

public interface IUserChat {

    void receiveMsn(String msn);
    String getName();

}
