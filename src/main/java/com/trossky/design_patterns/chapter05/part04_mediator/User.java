package com.trossky.design_patterns.chapter05.part04_mediator;

public class  User implements  IUserChat{
    private String name;

    public User(String name) {
        this.name = name;
    }

    public void receiveMsn(String msn) {
        System.out.println(msn);
    }
    public String getName() {
        return name;
    }
}
