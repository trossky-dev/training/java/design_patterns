package com.trossky.design_patterns.chapter05.part09_template_method;

/**
 * Father class
 * The algorithm in common for all subclasses is defined in authentication() method.
 */
public abstract class User {
    public void authentication(){
        System.out.println("All users need are authenticated");
    }
    // The subclasses must defined the behavioral
    abstract void wayWork();
}
