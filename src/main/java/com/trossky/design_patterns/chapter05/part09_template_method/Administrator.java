package com.trossky.design_patterns.chapter05.part09_template_method;

public class Administrator extends User{
    @Override
    void wayWork() {
        System.out.println("The way work to a Administrator");

    }
}
