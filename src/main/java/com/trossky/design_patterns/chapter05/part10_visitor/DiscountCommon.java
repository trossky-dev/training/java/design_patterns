package com.trossky.design_patterns.chapter05.part10_visitor;

/**
 * Acà aplicamos los algoritmos a los objetos,
 * entonces los objetos deconoceran las operaciones
 * que se programan, para este caso las operaciones
 * para el descuento.
 */
public class DiscountCommon  implements IVisitor{
    @Override
    public float visit(IFruit iFruit) {
        return iFruit.getPrice()*0.10f;
    }

    @Override
    public float visit(IWhiteLine iWhiteLine) {
        return iWhiteLine.getPrice()*0.05f;
    }

    @Override
    public float visit(IProduct iProduct) {
        return 0;
    }
}
