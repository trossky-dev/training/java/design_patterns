package com.trossky.design_patterns.chapter05.part10_visitor;

public class WashingMachine implements IWhiteLine, IVisitable{

    @Override
    public float getPrice() {
        return 40f;
    }

    @Override
    public float applyDiscount(IVisitor visitor) {
        return visitor.visit(this);
    }
}
