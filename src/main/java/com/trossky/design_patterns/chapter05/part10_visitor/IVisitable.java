package com.trossky.design_patterns.chapter05.part10_visitor;

/**
 * Permit apply discount to any product
 */
public interface IVisitable {

    float applyDiscount(IVisitor visitor);
}
