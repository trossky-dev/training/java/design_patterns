package com.trossky.design_patterns.chapter05.part10_visitor;

public interface IVisitor {
    float visit(IFruit iFruit);
    float visit(IWhiteLine iWhiteLine);
    float visit(IProduct iProduct);

}
