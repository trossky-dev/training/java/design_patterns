package com.trossky.design_patterns.chapter05.part08_strategy;


import com.trossky.design_patterns.chapter05.part01_chain_responsibility.ITransactionHandler;
import com.trossky.design_patterns.chapter05.part02_command.ICommand;
import com.trossky.design_patterns.chapter05.part04_mediator.RoomChat;
import com.trossky.design_patterns.chapter05.part06_observer.IObservable;
import com.trossky.design_patterns.chapter05.part06_observer.IObserver;
import com.trossky.design_patterns.chapter05.part07_state.CarMovement;
import com.trossky.design_patterns.chapter05.part07_state.CarOff;
import com.trossky.design_patterns.chapter05.part07_state.CarStart;
import com.trossky.design_patterns.chapter05.part07_state.ICarState;


/***********************************************************************************************************************
 *                                    DESIGN PATTERNS FOR BEHAVIORAL OBJECTS
 *----------------------------------------------------------------------------------------------------------------------
 * -- [CR]-Chain Responsibility --
 *               Avoid coupling the sender of a request ti its receiver by giving more than one object a
 *               change to handle the resquest.
 *               Chainthe receiving objects and pass the resquest along the chain until an object handles it.
 *               Example:
 *               Permite establecer una cadena de objetos receptores, la cual debe ser capaz de recibir
 *               información de un objeto emisor.
 *               Es decir el Cliente envia la petición al objeto número 1.
 *                  Sí el objeto número 1, no es capaz de responder a la petición, entonces delegara la tarea
 *                  al objeto número 2.
 *                  Ahora el Objeto número 2 intentara responder de forma correcta, pero si falla entonces delegara
 *                  al Objeto número 3. Y asi sucesivamente se delegara la responsabilidad hasta que algún objeto
 *                  logre responder satisfactoriamente.TR
 *               See class {@link ITransactionHandler}
 *
 * -- [C]-Command --
 *               Encapsulate a request as an object, thereby letting parameterize clients with different requests,
 *               queue or log requests, and support undoable operations.
 *               Example:
 *               Desacoplar al objeto que invoca la operaciòn.
 *               See class {@link ICommand}
 *
 * -- [I]-Iterator --
 *               Provide a way to access the elements of an aggregate object sequentially without exposing
 *               its underlying representation.
 *               Example:
 *               Para no exponer los atributos y metodos de la colección.
 *               See class {@link }
 *
 * -- [M]-Mediator --
 *               Define an object that encapsulates hw a set of objects interact.
 *               Mediator promotes loose coupling by keeping objects from referring
 *               to each explicity, and it lets you vary their interaction independently.
 *               Example:
 *               Una torre de control en un aeropuerto, la torre de control es el mediador para
 *               que los aviones essten en comunicación
 *               See class {@link RoomChat}
 *
 * -- [ME]-Memento --
 *               Without violating encapsulation, capture and externalize an object's internal state
 *               so that the object can be restored to this state later.
 *               Example:
 *               Se puede implementar el Ctl+z en nuestra app. Le permite crear una copia de seguridad
 *               completa o parcial del objeto para posteriormente poder revertir cambios.
 *               See class {@link com.trossky.design_patterns.chapter05.part05_memento.User}
 *
 * -- [O]-Observer --
 *               Define a one-to-many dependency between objects so that when one object change state,
 *               all its dependents are notified and updated automatically.
 *               Example:
 *               Siempre que un producto se venda, sera notificada a todos los administradores
 *               Tambien conocido como --PUBSUB--
 *               See class {@link IObserver} {@link IObservable}
 *
 * -- [S]-State --
 *               Allow an object to alter its behavior when its internal state changes.
 *               The object will appear to change its class.
 *               Example:
 *               Only when you require state machine in your projects.
 *               This example use car object, It have the states as: Start, in Movement and Off.
 *               See class {@link ICarState} {@link CarStart} {@link CarMovement} {@link CarOff}
 *
 * -- [ST]-Strategy --
 *               Define a family of algorithms, encapsulate each one, and make them interchangeable.
 *               Strategy lets the algorithm vary independently from clients taht use it.
 *               Example:
 *               Encapsulate algorithm in class, as a part of lego. In runtime.
 *               This patterns is used when an algorithm change lot of in an system.
 *               Operations for calculate commisiones, Its change for each user.
 *               Note: An Class for each algorithm. this example have two algorithm.
 *               See class {@link IStrategy} {@link Deposit} {@link Withdrawal}
 */
public class STMain {

    /**

     *
     * Now see as example, an bank transaction using two simple algorithms.
     * 1. Deposit
     * 2. Withdrawal
     */
    public static void main(String[] args) {
        System.out.println("START EG ");
        System.out.println("START Strategy");

        Transaction transaction1 = new Transaction(new Deposit());
        System.out.println(transaction1.executeTransaction(100,20));

        Transaction transaction2 = new Transaction(new Withdrawal());
        System.out.println(transaction2.executeTransaction(100,20));
        System.out.println("END Strategy");
    }

}
















