package com.trossky.design_patterns.chapter05.part08_strategy;

/**
 * This interface must defined the methods where are the algorithms.
 */
public interface IStrategy {
    float executeOperation(float balance, float quantity);
}
