package com.trossky.design_patterns.chapter05.part08_strategy;

public class Transaction  {
    private float quantity;
    private float balance;
    private IStrategy strategy; // Have an algorithm

    public Transaction(IStrategy strategy) {
        this.strategy = strategy;
    }
    public float executeTransaction(float balance, float quantity) {
        return this.strategy.executeOperation(balance, quantity);
    }



}
