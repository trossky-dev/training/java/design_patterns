package com.trossky.design_patterns.chapter05.part08_strategy;

public class Deposit implements IStrategy{
    @Override
    public float executeOperation(float balance, float quantity) {
        return balance+quantity;
    }
}
