package com.trossky.design_patterns.chapter05.part03_iterator;

public class GuideIterator implements Iterator {
    private PhoneGuide phoneGuide;
    private int position;

    public GuideIterator(PhoneGuide phoneGuide) {
        this.phoneGuide = phoneGuide;
    }

    @Override
    public String next() {
        return phoneGuide.getNumbers().get(position++);
    }

    @Override
    public boolean containerNext() {
        return position < phoneGuide.getNumbers().size()
                && phoneGuide.getNumbers().get(position) != null;
    }
}
