package com.trossky.design_patterns.chapter05.part03_iterator;

import java.util.ArrayList;

public class PhoneGuide {
    private ArrayList<String> numbers;

    public PhoneGuide() {
        this.numbers = new ArrayList<String>();
    }

    public void add(String number) {
        this.numbers.add(number);
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }
}
