package com.trossky.design_patterns.chapter05.part03_iterator;

public interface Iterator {
    String next();

    boolean containerNext();
}
