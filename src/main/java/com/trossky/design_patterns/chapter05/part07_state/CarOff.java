package com.trossky.design_patterns.chapter05.part07_state;

public class CarOff implements ICarState{
    private final Car car;

    public CarOff(Car car) {
        this.car = car;
    }

    @Override
    public void start() {
        System.out.println("The car is start !");
        car.setCurrentState(car.getCarStart());
    }

    @Override
    public void inMovement() {
        System.out.println("The car can't are in movement, If Car is off!");
    }

    @Override
    public void off() {
        System.out.println("The car already is off");

    }
}

