package com.trossky.design_patterns.chapter05.part07_state;

public class Car implements ICarState{

    private ICarState carStart;
    private ICarState carMovement;
    private ICarState carOff;

    private ICarState currentState;

    public Car() {
        carStart = new CarStart(this);
        carMovement = new CarMovement(this);
        carOff = new CarOff(this);

        //currentState =  carStart;
    }

    @Override
    public void start() {
        this.currentState.start();
    }

    @Override
    public void inMovement() {
        this.currentState.inMovement();
    }

    @Override
    public void off() {
        this.currentState.off();
    }

    public ICarState getCarStart() {
        return carStart;
    }

    public void setCarStart(ICarState carStart) {
        this.carStart = carStart;
    }

    public ICarState getCarMovement() {
        return carMovement;
    }

    public void setCarMovement(ICarState carMovement) {
        this.carMovement = carMovement;
    }

    public ICarState getCarOff() {
        return carOff;
    }

    public void setCarOff(ICarState carOff) {
        this.carOff = carOff;
    }

    public ICarState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(ICarState currentState) {
        this.currentState = currentState;
    }
}
