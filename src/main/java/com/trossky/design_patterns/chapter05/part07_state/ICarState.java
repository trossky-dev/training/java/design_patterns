package com.trossky.design_patterns.chapter05.part07_state;
// By each state, You must generate an class
public interface ICarState {

    void start();
    // Handle car
    void inMovement();

    void off();
}
