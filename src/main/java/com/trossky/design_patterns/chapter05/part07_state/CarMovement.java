package com.trossky.design_patterns.chapter05.part07_state;

public class CarMovement implements ICarState{
    private final Car car;

    public CarMovement(Car car) {
        this.car = car;
    }

    @Override
    public void start() {
        System.out.println("The car is already start!");
    }

    @Override
    public void inMovement() {
        System.out.println("The car already is in movement");

    }

    @Override
    public void off() {
        System.out.println("The car is off");
        car.setCurrentState(car.getCarOff());
    }
}

