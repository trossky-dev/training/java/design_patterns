package com.trossky.design_patterns.chapter05.part07_state;

public class  CarStart implements ICarState{
    private final Car car;

    public CarStart(Car car) {
        this.car = car;
    }

    @Override
    public void start() {
        System.out.println("The car is start!");
    }

    @Override
    public void inMovement() {
        System.out.println("The car is in movement");
        car.setCurrentState(car.getCarMovement());
    }

    @Override
    public void off() {
        System.out.println("The car is off");
        car.setCurrentState(car.getCarOff());
    }
}

